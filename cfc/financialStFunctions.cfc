<!--- FinancialSt Functions --->
<cfcomponent>
	<cfset finman = CreateObject("cfc\financialStManager") />

	<!--- This function returns a list of all financial statement below the specified node  --->
	<cffunction name="Get_FS_All_Depth" output="true" access= "public" returnType="array">
		<cfargument name="FSID" type="string" required="true" default="" />
		<cfset var FSData = finman.Get_FS_Data_full(#arguments.FSID#)>

		<cfif IsDefined("FSData.DBASSET_CHILDFS_ID")>
			<cfset var childFS_ID = #FSData.DBASSET_CHILDFS_ID#>
		<cfelseif IsDefined("FSData.DBLIABILITY_CHILDFS_ID")>
			<cfset var childFS_ID = #FSData.DBLIABILITY_CHILDFS_ID#>
		</cfif>
		<cfset var FinancialStIDs = [childFS_ID]>

		<!--- lookfor sub assets or liabilities then repeat --->
		<cfset var assetsOfasset = finman.get_assets(#childFS_ID#)>
		<cfif #assetsOfasset.recordcount# GT 0>
			<cfloop query="assetsOfasset">
				<cfset ArrayAppend(FinancialStIDs, Get_FS_All_Depth(#DBASSET_CHILDFS_ID#), true)>
			</cfloop>
		</cfif>	

		<cfset var liabilitiesOfasset = finman.get_liabilities(#childFS_ID#)>
		<cfif #liabilitiesOfasset.recordcount# GT 0>	
			<cfloop query="liabilitiesOfasset">
				<cfset ArrayAppend(FinancialStIDs, Get_FS_All_Depth(#DBLIABILITY_CHILDFS_ID#), true)>
			</cfloop>
		</cfif>	

		<!--- Return --->
		<cfreturn #FinancialStIDs#>	
		
	</cffunction>

<!--- This function returns a list of all incomes below the specified node  --->
<cffunction name="Get_Incomes_All_Depth" output="true" access= "public" returnType="query">
		<cfargument name="FSID" type="string" required="true" default="" />
		<cfset FSIDs = Get_FS_All_Depth(#arguments.FSID#) />

		<cfset myQuery = finman.get_incomes(0, true)>
		<cfloop index = "FSid" array = "#FSIDs#">
			<cfset incomes = finman.get_incomes(#FSid#, true)>
			<cfset myQuery = QueryAppend(#myQuery#,#incomes#)>
		</cfloop>

		<cfreturn #myQuery#>	
</cffunction>

<!--- This function the total sum of all incomes below the specified node  --->
<cffunction name="Get_TotalIncome_All_Depth" output="true" access= "public" returnType="string">
		<cfargument name="FSID" type="string" required="true" default="" />

		<cfset Var myIncomes = Get_Incomes_All_Depth(#arguments.FSID#)>
		
		<cfquery dbtype="query" name="totalIncomes">
			SELECT SUM(DBINCOME_AMOUNT) AS totalAssetIncome FROM myIncomes
		</cfquery>

		<cfif IsNumeric(#totalIncomes.totalAssetIncome#)>
			<cfreturn #totalIncomes.totalAssetIncome#>	
		<cfelse>
			<cfreturn 0>
		</cfif>
</cffunction>

<!--- This function returns the net income all incomes-expenses below the specified node  --->
<cffunction name="Get_NetIncome_All_Depth" output="true" access= "public" returnType="string">
		<cfargument name="FSID" type="string" required="true" default="" />
		<cfset Var myIncomes = Get_TotalIncome_All_Depth(#arguments.FSID#)>
		<cfset Var myExpenses = Get_TotalExpense_All_Depth(#arguments.FSID#)>
		<cfif NOT IsNumeric(myIncomes)>
			<cfset Var myIncomes = 0>
		</cfif>
		<cfif NOT IsNumeric(myExpenses)>
			<cfset Var myExpenses = 0>
		</cfif>
		<cfreturn #myIncomes# - #myExpenses#>	
</cffunction>

<!--- This function returns a list of all expenses below the specified node  --->
<cffunction name="Get_Expenses_All_Depth" output="true" access= "public" returnType="query">
		<cfargument name="FSID" type="string" required="true" default="" />
		<cfset FSIDs = Get_FS_All_Depth(#arguments.FSID#) />
		<cfset myQuery = finman.get_expenses(0,true)>
		<cfloop index = "FSid" array = "#FSIDs#">
			<cfset incomes = finman.get_expenses(#FSid#, true)>
			<cfset myQuery = QueryAppend(#myQuery#,#incomes#)>
		</cfloop>
		<cfreturn #myQuery#>	
</cffunction>


<!--- This function returns the total sum of all expenses below the specified node  --->
<cffunction name="Get_TotalExpense_All_Depth" output="true" access= "public" returnType="string">
		<cfargument name="FSID" type="string" required="true" default="" />

		<cfset Var myExpenses = Get_Expenses_All_Depth(#arguments.FSID#)>
		<cfquery name="totalExpenses" dbtype="query">
			SELECT SUM(DBEXPENSE_AMOUNT) AS totalAssetExpenses FROM myExpenses
		</cfquery>

		<cfif IsNumeric(#totalExpenses.totalAssetExpenses#)>
			<cfreturn #totalExpenses.totalAssetExpenses#>	
		<cfelse>
			<cfreturn 0>
		</cfif>
</cffunction>

<!--- Asset Functions --->

<!--- This function returns a list of all assets below the specified node  --->
<cffunction name="Get_Assets_All_Depth" output="false" access= "public" returnType="query">
		<cfargument name="FSID" type="string" required="true" default="" />
		<cfset FSIDs = Get_FS_All_Depth(#arguments.FSID#) />
		<cfset myQuery = finman.Retrieve_asset_data(#arguments.FSID#)>
		
		<cfloop index = "FSid" array = "#FSIDs#">
			<cfset assets = finman.Get_assets(#FSid#)>
			<cfset myQuery = QueryAppend(#myQuery#,#assets#)>
		</cfloop>
		<cfreturn #myQuery#>	
</cffunction>

<cffunction name="Get_Assets_E_D_All_Depth" output="false" access= "public" returnType="any">
		<cfargument name="FSID" type="string" required="true" default="" />
		<cfset Var myAssets = Get_Assets_All_Depth(#FSID#)>

		<cfquery name="totalE" dbtype="query">
			SELECT SUM(DBASSET_EQUITY) AS totalAssetEquity FROM myQuery;
		</cfquery>
		<cfquery name="totalD" dbtype="query">
			SELECT SUM(DBASSET_DEBT) AS totalAssetDebt FROM myQuery;
		</cfquery>

		<cfif IsNumeric(#totalE.totalAssetEquity#) AND IsNumeric(#totalD.totalAssetDebt#)>
			<cfreturn {assetEquity = #totalE.totalAssetEquity#, assetDebt = #totalD.totalAssetDebt#}>	
		<cfelse>
			<cfreturn {assetEquity = 0, assetDebt = 0}>
		</cfif>
</cffunction>

<!--- Liabilities Functions --->

<!--- This function returns a list of all Liabilities below the specified node  --->
<cffunction name="Get_Liabilities_All_Depth" output="false" access= "public" returnType="query">
		<cfargument name="FSID" type="string" required="true" default="" />
		<cfset FSIDs = Get_FS_All_Depth(#arguments.FSID#) />
		<cfset myQuery = finman.Get_liabilities(-1)>
		
		<cfloop index = "FSid" array = "#FSIDs#">
			<cfset incomes = finman.Get_liabilities(#FSid#)>
			<cfset myQuery = QueryAppend(#myQuery#,#incomes#)>
		</cfloop>
		<cfreturn #myQuery#>	
</cffunction>


<cffunction name="Get_Liabilities_E_D_All_Depth" output="false" access= "public" returnType="any">
		<cfargument name="FSID" type="string" required="true" default="" />
		<cfset Var myLiabilities = Get_Liabilities_All_Depth(#FSID#)>

		<cfquery name="totalE" dbtype="query">
			SELECT SUM(dbLiability_Equity) AS totalLiabilityE FROM myLiabilities
		</cfquery>
		<cfquery name="totalD" dbtype="query">
			SELECT SUM(dbLiability_Debt) AS totalLiabilityD FROM myLiabilities
		</cfquery>

		<cfif IsNumeric(#totalE.totalLiabilityE#) AND IsNumeric(#totalD.totalLiabilityD#)>
			<cfreturn {liabilityEquity = #totalE.totalLiabilityE#, liabilityDebt = #totalD.totalLiabilityD#}>	
		<cfelse>
			<cfreturn {liabilityEquity = 0, liabilityDebt = 0}>
		</cfif>
</cffunction>

<!--- other help functions --->
<cffunction name="QueryAppend" access="public" returntype="query" output="false"
    hint="This takes two queries and appends the second one to the first one. Returns the resultant third query.">
 
    <!--- Define arguments. --->
    <cfargument name="QueryOne" type="query" required="true" />
    <cfargument name="QueryTwo" type="query" required="true" />
    <cfargument name="UnionAll" type="boolean" required="false" default="true" />
 
    <!--- Define the local scope. --->
    <cfset var LOCAL = StructNew() />
 
    <!--- Append the second to the first. Do this by unioning the two queries. --->
    <cfquery name="LOCAL.NewQuery" dbtype="query">
        SELECT * FROM ARGUMENTS.QueryOne UNION
        <cfif ARGUMENTS.UnionAll>
            ALL
        </cfif>
        SELECT * FROM ARGUMENTS.QueryTwo
    </cfquery>
 
    <!--- Return the new query. --->
    <cfreturn LOCAL.NewQuery />
</cffunction>

</cfcomponent>