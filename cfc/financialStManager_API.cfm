<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - <text>
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 26/09/2014
////////////////////////////////////////////////////// --->
<cfset finman = CreateObject("cfc\financialStManager") />

<cfif #form.FSMFunction# EQ "IncomeStatement_Add_Income">
	<cfif IsDefined("form.INCOMENAME")>
		<cfset incomeID = finman.Create_income(#form.INCOMENAME#, #form.FINANCIALST_ID#, #form.INCOMEDESCRIPTION#, #form.INCOMEAMOUNT#, #form.INCOMEDATE#)>
		<span class="glyphicon glyphicon-ok"></span> Your Income, with ID: <cfoutput>#incomeID#</cfoutput>, was successfuly added to te Database!
	</cfif>
</cfif><!---./IncomeStatement_Add_Income --->

<cfif #form.FSMFunction# EQ "IncomeStatement_Update_Income">
	<cfif IsDefined("form.INCOMENAME")>
		<cfif IsDefined("form.INCOME_ID")>
			<cfset incomeUpdate = finman.Update_income(incomeID = #form.Income_ID#, incomeName = #form.INCOMENAME#, incomeDescription = #form.INCOMEDESCRIPTION#, incomeAmount = #form.INCOMEAMOUNT#, incomeDate = #form.INCOMEDATE#)>
			
			<span class="glyphicon glyphicon-ok"></span> Your Income, with ID: <cfoutput>incomeID</cfoutput>, was successfuly updated!
		</cfif>
	</cfif>
</cfif><!---./IncomeStatement_Update_Income --->

<cfif #form.FSMFunction# EQ "IncomeStatement_Add_Expense">
	<cfif IsDefined("form.EXPENSENAME")>
		<cfset expenseID = finman.Create_expense(#form.EXPENSENAME#, #form.FINANCIALST_ID#, #form.EXPENSEDESCRIPTION#, #form.EXPENSEAMOUNT#, #form.EXPENSEDATE#)>
		<span class="glyphicon glyphicon-ok"></span> Your Expense, with ID: <cfoutput>#expenseID#</cfoutput>, was successfuly added to te Database!

	</cfif>
</cfif><!---./IncomeStatement_Add_Expense --->

<cfif #form.FSMFunction# EQ "IncomeStatement_Update_Expense">
	<cfif IsDefined("form.EXPENSENAME")>
		<cfif IsDefined("form.EXPENSE_ID")>
			<cfset expenseUpdate = finman.Update_expense(expenseID = #form.expense_ID#, expenseName = #form.expenseNAME#, expenseDescription = #form.expenseDESCRIPTION#, expenseAmount = #form.expenseAMOUNT#, expenseDate = #form.expenseDATE#)>

			<span class="glyphicon glyphicon-ok"></span> Your Expense, with ID: <cfoutput>expenseID</cfoutput>, was successfuly updated!
		</cfif>
	</cfif>
</cfif><!---./IncomeStatement_Update_Expense --->


<cfif #form.FSMFunction# EQ "IncomeStatement_Delete_Income_Expense">
	<!--- Type: 1 = income, 2 = expense --->
	<cfif #session.user.userTempVar1# EQ "income">
		<cfset return = finman.Delete_income(#session.user.userTempVar2#)>
	<cfelseif #session.user.userTempVar1# EQ "expense">
		<cfset return = finman.Delete_expense(#session.user.userTempVar2#)>
	</cfif>
	<cfset session.user.userTempVar1 = 0>
	<cfset session.user.userTempVar2 = 0>
	<cfif return EQ 1>
		<span class="glyphicon glyphicon-ok"></span> Deleted Successfully!
	</cfif>
</cfif><!---./IncomeStatement_Delete_Income_Expense --->

<cfif #form.FSMFunction# EQ "BalanceSheet_Add_Asset">
	<!---  in case of call of posting an asset, create asset. --->
	<cfif IsDefined("form.assetName")>
		<cfset assetID = finman.Create_asset(assetName = #form.assetName#, assetParentFSID = #form.FINANCIALST_ID#, assetDescription = #form.assetDescription#, assetEquity = #form.assetEquity#, assetDebt = #form.assetDebt#, assetDate = #form.assetDATE#)>
			
		<span class="glyphicon glyphicon-ok"></span> Your asset, with ID: <cfoutput>#assetID#</cfoutput>, was successfuly added to te Database.
	</cfif>
</cfif><!---./BalanceSheet_Add_Asset --->


<cfif #form.FSMFunction# EQ "BalanceSheet_Update_Asset">
	<!---  in case of call of posting an asset, create asset. --->
	<cfif IsDefined("form.ASSET_ID")>
		<cfset assetUpdateOk = finman.Update_asset(assetID = #form.ASSET_ID#, assetName = #form.Name#, assetDescription = #form.Description#, assetEquity = #form.Equity#, assetDebt = #form.Debt#, assetDate = #form.Date#)>
		<span class="glyphicon glyphicon-ok"></span> Your asset, with ID: <cfoutput>#form.ASSET_ID#</cfoutput>, was successfuly updated.

		<cfset getFS = finman.Retrieve_FS_data(#session.user.userCurrentFSID#)>
		<script type="text/javascript">
			$('#FSTitleSpan').html('<cfoutput>#getFS.DBFINANCIALST_NAME#</cfoutput>');
		</script>
	</cfif>
</cfif><!---./BalanceSheet_Add_Asset --->

<cfif #form.FSMFunction# EQ "BalanceSheet_Add_Liability">
	<!---  in case of call of posting an liability, create liability. --->
	<cfif IsDefined("form.liabilityNAME")>
		<cfset liabilityID = finman.Create_liability(liabilityName = #form.liabilityNAME#, liabilityParentFSID = #form.FINANCIALST_ID#, liabilityDescription = #form.liabilityDESCRIPTION#, liabilityEquity = #form.liabilityEquity#, liabilityDebt = #form.liabilityDebt#, liabilityDate = #form.liabilityDATE#)>
			
		<span class="glyphicon glyphicon-ok"></span> Your liability, with ID: <cfoutput>#liabilityID#</cfoutput>, was successfuly added to te Database.
	</cfif>
</cfif><!---./BalanceSheet_Add_Liability --->

<cfif #form.FSMFunction# EQ "BalanceSheet_Update_Liability">
	<!---  in case of call of posting an liability, create liability. --->
	<cfif IsDefined("form.LIABILITY_ID")>
		<cfset liabilityUpdateOk = finman.Update_liability(liabilityID = #form.LIABILITY_ID#, liabilityName = #form.Name#, liabilityDescription = #form.Description#, liabilityEquity = #form.Equity#, liabilityDebt = #form.Debt#, liabilityDate = #form.Date#)>
		<span class="glyphicon glyphicon-ok"></span> Your liability, with ID: <cfoutput>#form.LIABILITY_ID#</cfoutput>, was successfuly updated.

		<cfset getFS = finman.Retrieve_FS_data(#session.user.userCurrentFSID#)>
		<script type="text/javascript">
			$('#FSTitleSpan').html('<cfoutput>#getFS.DBFINANCIALST_NAME#</cfoutput>');
		</script>
	</cfif>
</cfif><!---./BalanceSheet_Update_Liability --->



<script type="text/javascript">
$( document ).ready(function() {
  	FinancialStatementLoad();
});
</script>