<!--- User Manager --->
<cfcomponent>
<cfset finStManager = CreateObject("cfc\financialStManager") />

	<cffunction name="Retrieve_user_data" output="false" access= "public" returnType="string">
	</cffunction>

	<!--- Create_user(userName, userEmail, userUserName, userPassword): querry --->
	<cffunction name="Create_user" output="true" access= "public" returnType="string">
		<cfargument name="userName" type="string" required="true" default="" />
		<cfargument name="userEmail" type="string" required="true" default="" />
		<cfargument name="userUserName" type="string" required="true" default="" />
		<cfargument name="userPassword" type="string" required="true" default="" />
		
		<!--- Create Root Asset --->
		<!--- Create_asset(assetName, assetParentFSID, assetDescription, assetEquity, assetDebt, assetDate): asset id --->
		<!--- Create_asset(assetName, assetParentFSID, assetDescription, assetCost): asset id--->
		<cfset rootAsset = finStManager.Create_asset(assetName = #arguments.userName#, assetParentFSID = 0, assetDescription= 'User Root Asset', assetEquity = 0, assetDebt = 0, assetDate = '28-09-2014')>
		<cfset rootAssetData = finStManager.Retrieve_asset_data(#rootAsset#)>
		data
		<cfdump var = #rootAssetData#>

		<!--- Create User --->
		<cfquery name="createUser" datasource="finfun_database">
			INSERT INTO dbUser (dbUser_Name, dbUser_Email, dbUser_UserName, dbUser_Password, dbUser_Class, 
								dbUser_CreationDate) 
			VALUES ( 			
				<cfqueryparam value="#arguments.userName#" cfsqltype="CF_SQL_VARCHAR" />, 
				<cfqueryparam value="#arguments.userEmail#" cfsqltype="CF_SQL_VARCHAR" />,
				<cfqueryparam value="#arguments.userUserName#" cfsqltype="CF_SQL_VARCHAR" />,
				SHA1(
				<cfqueryparam value="#arguments.userPassword#" cfsqltype="CF_SQL_VARCHAR" />
				),
				1,
				CURDATE()
			);
		</cfquery>

		<cfquery name="returnID" datasource="finfun_database" >
			SELECT LAST_INSERT_ID() AS ID;			
		</cfquery>

		<cfquery name="user_asset_link" datasource="finfun_database" >
			INSERT INTO dbUser_FinancialSt_link (dbUser_FinancialSt_link_User_ID, dbUser_FinancialSt_link_RootFS_ID)
			VALUES ( 
				<cfqueryparam value="#returnID.ID[1]#" cfsqltype="cf_sql_integer" />,
				<cfqueryparam value="#rootAssetData.dbAsset_ChildFS_ID#" cfsqltype="cf_sql_integer" />
			)	
		</cfquery>

		<!--- Return --->
		<cfreturn #returnID.ID[1]#/>
	</cffunction>


</cfcomponent>