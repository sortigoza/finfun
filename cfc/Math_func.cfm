<cfscript>
	/**
	 * Returns the standard deviation calculated using the divisor n method.
	 * 
	 * @param values      Comma delimited list or one dimensional array of numeric values. 
	 * @return Returns a simple value. 
	 * @author Rob Brooks-Bilson (rbils@amkor.com) 
	 * @version 1.1, September 7, 2001 
	 */
	function StdDevPop(values)
	{
	  Var MyArray = 0;
	  Var NumValues = 0;
	  Var xBar = 0;
	  Var SumxBar = 0;  
	  Var i=0;
	  if (IsArray(values)){
	     MyArray = values;
	    }
	  else {
	     MyArray = ListToArray(values);
	    }
	  NumValues = ArrayLen(MyArray);
	  xBar = ArrayAvg(MyArray);
	  for (i=1; i LTE NumValues; i=i+1) {
	    SumxBar = SumxBar + ((MyArray[i] - xBar)*(MyArray[i] - xBar));
	    }
	  Return Sqr(SumxBar/NumValues);
	}
	</cfscript>

	<cfscript>
	/**
	 * Returns the median (middle) value for a set of numberic values.
	 * 
	 * @param values      Comma delimited list or one dimensional array of numeric values. 
	 * @return Returns a simple value. 
	 * @author Rob Brooks-Bilson (rbils@amkor.com) 
	 * @version 1.0, July 18, 2001 
	 */
	function Median_private(values)
	{
	  Var x = 0;
	  Var NumberOfElements = 0;
	  Var LeftCenterPosition = 0;
	  Var RightCenterPosition = 0;
	  Var MyArray = 0;
	  if (IsArray(values)){
	     MyArray = values;
	    }
	  else {
	     MyArray = ListToArray(values);
	    }
	  ArraySort(MyArray, "numeric");
	  x = ArrayToList(MyArray);
	  NumberOfElements = ListLen(x);
	  if ((NumberOfElements MOD 2) EQ 0) {
	      LeftCenterPosition = ListGetAt(x, (Int(NumberOfElements/2)), ",");
	      RightCenterPosition = ListGetAt(x, (Int(NumberOfElements/2)+1), ",");
	      Return (LeftCenterPosition + RightCenterPosition)/2;
	    }
	  else {
	      Return ListGetAt(x, Int(NumberOfElements/2)+1, ",");
	    }
	}
	</cfscript>