<!--- FinancialSt Manager --->
<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - FinancialSt Manager: This component purpose is to create an abstraction layer to the financial statement data inside the Database, providing functions to use it e.g. Retrive, Add, Update and Delete.
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 31/05/2014
////////////////////////////////////////////////////// --->
<cfcomponent>
	<!--- ##### Financial Statement Manager ##### --->
	<!--- Retrieve_FS_data(FSID): Query --->
	<cffunction name="Retrieve_FS_data" output="false" access= "public" returnType="query">
		<cfargument name="FSID" type="string" required="true" default="" />

		<cfquery name="FSData" datasource="finfun_database" >
			SELECT * FROM dbFinancialSt WHERE dbFinancialSt_ID = 
				<cfqueryparam value="#arguments.FSID#" cfsqltype="cf_sql_integer" />
			;			
		</cfquery>

		<cfreturn #FSData#>
	</cffunction>

	<!--- Get_FS_Data_full(FSID): Query --->
	<cffunction name="Get_FS_Data_full" output="false" access= "public" returnType="query">
		<cfargument name="FSID" type="string" required="true" default="" />

		<cfquery name="assetTry" datasource="finfun_database">
			SELECT * FROM dbAsset WHERE dbAsset_ChildFS_ID = 
			<cfqueryparam value="#arguments.FSID#" cfsqltype="cf_sql_integer" />;		
		</cfquery>

		<cfif #assetTry.RecordCount#>
			<cfreturn #assetTry#>
		<cfelse>
			<cfquery name="LiabilityTry" datasource="finfun_database">
				SELECT * FROM dbLiability WHERE dbLiability_ChildFS_ID = 
				<cfqueryparam value="#arguments.FSID#" cfsqltype="cf_sql_integer" />;		
			</cfquery>
			<cfreturn #LiabilityTry#>
		</cfif>
	</cffunction>

	<!--- Create_FS(name, parentFS_ID): financialSt_ID --->
	<cffunction name="Create_FS" output="false" access= "public" returnType="string">
		<!--- Variables init--->
		<cfargument name="fsName" type="string" required="true" default="" />
		<cfargument name="fsParentID" type="string" required="true" default="" />

		<!--- Processes--->
		<cfquery name="createFS" datasource="finfun_database">
			INSERT INTO dbFinancialSt (dbFinancialSt_Name, dbFinancialSt_ParentFS_ID , dbFinancialSt_CreationDate) 
			VALUES ( 
				<cfqueryparam value="#arguments.fsName#" cfsqltype="CF_SQL_VARCHAR" />, 
				<cfqueryparam value="#arguments.fsParentID#" cfsqltype="cf_sql_integer" />,
				CURDATE()
			);
		</cfquery>

		<cfquery name="returnID" datasource="finfun_database" >
			SELECT LAST_INSERT_ID() AS ID;			
		</cfquery>

		<!--- Return --->
		<cfreturn #returnID.ID[1]#/>
	</cffunction>

	<!--- Delete_FS(FSID): 1 --->
	<cffunction name="Delete_FS" output="false" access= "public" returnType="string">
		<cfargument name="FSID" type="string" required="true" default="" />

		<cfquery name="deleteFS" datasource="finfun_database">
			DELETE FROM dbFinancialSt WHERE dbFinancialSt_ID =
				<cfqueryparam value="#arguments.FSID#" cfsqltype="cf_sql_integer" />
				LIMIT 1
			;			
		</cfquery>

		<!--- Return --->
		<cfreturn 1/>
	</cffunction>



	<!--- ##### Asset Manager ##### --->

	<!--- Retrieve_asset_data(assetID): query --->
	<cffunction name="Retrieve_asset_data" output="false" access= "public" returnType="query">
		<cfargument name="assetID" type="string" required="true" default="" />

		<cfquery name="assetData" datasource="finfun_database" >
			SELECT * FROM dbAsset WHERE dbAsset_ID = 
				<cfqueryparam value="#arguments.assetID#" cfsqltype="cf_sql_integer" />
			;			
		</cfquery>
		<cfreturn #assetData#>
	</cffunction>

	<!--- Get_assets(ParentFSID, From, To): query --->
	<cffunction name="Get_assets" output="false" access= "public" returnType="query">
		<cfargument name="ParentFSID" type="string" required="true" default="" />
		<cfargument name="From" type="string" required="false" default="" />
		<cfargument name="To" type="string" required="false" default="" />

		<cfquery name="assetData" datasource="finfun_database" >
			SELECT * FROM dbAsset WHERE dbAsset_FinancialSt_ID = 
				<cfqueryparam value="#arguments.ParentFSID#" cfsqltype="cf_sql_integer" />
			;			
		</cfquery>
		<cfreturn #assetData#>
	</cffunction>

	<!---  Update_asset(assetID, assetName, assetDescription, assetEquity, assetDebt, assetDate): 1 --->
	<cffunction name="Update_asset" output="false" access= "public" returnType="string">
		<cfargument name="assetID" type="string" required="true" default="" />
		<cfargument name="assetName" type="string" required="true" default="" />
		<cfargument name="assetDescription" type="string" required="true" default="" />
		<cfargument name="assetEquity" type="string" required="false" default="" />
		<cfargument name="assetDebt" type="string" required="false" default="" />
		<cfargument name="assetDate" type="string" required="false" default="" />

		<cfset assetData = Retrieve_asset_data("#arguments.assetID#")>

		<cfquery name="updateasset1" datasource="finfun_database">
			UPDATE dbFinancialSt SET
				dbFinancialSt_Name = <cfqueryparam value="#arguments.assetName#" cfsqltype="CF_SQL_VARCHAR" />
			WHERE
				dbFinancialSt_ID = <cfqueryparam value="#assetData.dbAsset_ChildFS_ID#" cfsqltype="cf_sql_integer" />
			LIMIT 1
			;
		</cfquery>

		<cfquery name="updateasset2" datasource="finfun_database">
			UPDATE dbAsset SET
				dbAsset_Name = <cfqueryparam value="#arguments.assetName#" cfsqltype="CF_SQL_VARCHAR" />, 
				dbAsset_Description = <cfqueryparam value="#arguments.assetDescription#" cfsqltype="CF_SQL_VARCHAR" />,
				dbAsset_Equity = <cfqueryparam value="#arguments.assetEquity#" cfsqltype="CF_SQL_REAL" />,
				dbAsset_Debt = <cfqueryparam value="#arguments.assetDebt#" cfsqltype="CF_SQL_REAL" />,
				dbAsset_CreationDate = <cfqueryparam value="#arguments.assetDate#" cfsqltype="CF_SQL_DATE" />
			WHERE
				dbAsset_ID = <cfqueryparam value="#arguments.assetID#" cfsqltype="cf_sql_integer" />
			LIMIT 1
			;
		</cfquery>

		<!--- Return --->
		<cfreturn 1/>
	</cffunction>

	<!--- Create_asset(assetName, assetParentFSID, assetDescription, assetEquity, assetDebt, assetDate): asset id --->
	<cffunction name="Create_asset" output="false" access= "public" returnType="string">
		<cfargument name="assetName" type="string" required="true" default="" />
		<cfargument name="assetParentFSID" type="string" required="true" default="" />
		<cfargument name="assetDescription" type="string" required="true" default="" />
		<cfargument name="assetEquity" type="string" required="true" default="" />
		<cfargument name="assetDebt" type="string" required="true" default="" />
		<cfargument name="assetDate" type="string" required="true" default="" />

		<cfset assetFSID = Create_FS(fsName = "#arguments.assetName#",fsParentID = "#arguments.assetParentFSID#")>

		<cfquery name="createasset" datasource="finfun_database">
			INSERT INTO dbAsset (dbAsset_Name, dbAsset_FinancialSt_ID ,dbAsset_ChildFS_ID , dbAsset_Description, dbAsset_Equity, dbAsset_Debt, dbAsset_Active, dbAsset_CreationDate) 
			VALUES (
				<cfqueryparam value="#arguments.assetName#" cfsqltype="CF_SQL_VARCHAR" />, 
				<cfqueryparam value="#arguments.assetParentFSID#" cfsqltype="cf_sql_integer" />,
				<cfqueryparam value="#assetFSID#" cfsqltype="cf_sql_integer" />, 
				<cfqueryparam value="#arguments.assetDescription#" cfsqltype="CF_SQL_VARCHAR" />,
				<cfqueryparam value="#arguments.assetEquity#" cfsqltype="CF_SQL_REAL" />, 
				<cfqueryparam value="#arguments.assetDebt#" cfsqltype="CF_SQL_REAL" />, 
				1,
				STR_TO_DATE(#DateFormat(arguments.assetDate , 'YYYY-MM-DD')#, '%Y-%m-%d')
			);
		</cfquery>

		<cfquery name="returnID" datasource="finfun_database" >
			SELECT LAST_INSERT_ID() AS ID;			
		</cfquery>

		<!--- Return --->
		<cfreturn #returnID.ID[1]#/>
	</cffunction>

	<!--- Delete_asset(assetID): 1 --->
	<cffunction name="Delete_asset" output="false" access= "public" returnType="string">
		<cfargument name="assetID" type="string" required="true" default="" />
		<!--- Missing: find sub FS and kill them!!!   --->
		<cfset assetData = Retrieve_asset_data("#arguments.assetID#")>
		<cfset deleteFSoK = Delete_FS("#assetData.dbAsset_ChildFS_ID#")>

		<cfquery name="deleteasset" datasource="finfun_database">
			DELETE FROM dbAsset WHERE dbAsset_ID =
				<cfqueryparam value="#arguments.assetID#" cfsqltype="cf_sql_integer" />
				LIMIT 1
			;			
		</cfquery>

		<!--- Return --->
		<cfreturn 1/>
	</cffunction>


	<!--- ##### Liability Manager ##### --->
	<!--- Retrieve_liability_data(liabilityID): Query--->
	<cffunction name="Retrieve_liability_data" output="false" access= "public" returnType="query">
		<cfargument name="liabilityID" type="string" required="true" default="" />

		<cfquery name="liabilityData" datasource="finfun_database" >
			SELECT * FROM dbLiability WHERE dbLiability_ID = 
				<cfqueryparam value="#arguments.liabilityID#" cfsqltype="cf_sql_integer" />
			;			
		</cfquery>
		<cfreturn #liabilityData#>
	</cffunction>

	<!--- Get_liabilities(ParentFSID, From, To): query --->
	<cffunction name="Get_liabilities" output="false" access= "public" returnType="query">
		<cfargument name="ParentFSID" type="string" required="true" default="" />

		<cfquery name="liabilityData" datasource="finfun_database" >
			SELECT * FROM dbLiability WHERE dbLiability_FinancialSt_ID = 
				<cfqueryparam value="#arguments.ParentFSID#" cfsqltype="cf_sql_integer" />
			;			
		</cfquery>
		<cfreturn #liabilityData#>
	</cffunction>

	<!---  Update_liability(liabilityID, liabilityName, liabilityDescription, liabilityEquity, liabilityDebt, liabilityDate): 1 --->
	<cffunction name="Update_liability" output="false" access= "public" returnType="string">
		<cfargument name="liabilityID" type="string" required="true" default="" />
		<cfargument name="liabilityName" type="string" required="true" default="" />
		<cfargument name="liabilityDescription" type="string" required="true" default="" />
		<cfargument name="liabilityEquity" type="string" required="false" default="" />
		<cfargument name="liabilityDebt" type="string" required="false" default="" />
		<cfargument name="liabilityDate" type="string" required="false" default="" />

		<cfset liabilityData = Retrieve_liability_data("#arguments.liabilityID#")>

		<cfquery name="updateliability1" datasource="finfun_database">
			UPDATE dbFinancialSt SET
				dbFinancialSt_Name = <cfqueryparam value="#arguments.liabilityName#" cfsqltype="CF_SQL_VARCHAR" />
			WHERE
				dbFinancialSt_ID = <cfqueryparam value="#liabilityData.dbLiability_ChildFS_ID#" cfsqltype="cf_sql_integer" />
			LIMIT 1
			;
		</cfquery>

		<cfquery name="updateliability" datasource="finfun_database">
			UPDATE dbLiability SET
				dbLiability_Name = <cfqueryparam value="#arguments.liabilityName#" cfsqltype="CF_SQL_VARCHAR" />, 
				dbLiability_Description = <cfqueryparam value="#arguments.liabilityDescription#" cfsqltype="CF_SQL_VARCHAR" />,
				dbLiability_Equity = <cfqueryparam value="#arguments.liabilityEquity#" cfsqltype="CF_SQL_REAL" />,
				dbLiability_Debt = <cfqueryparam value="#arguments.liabilityDebt#" cfsqltype="CF_SQL_REAL" />,
				dbLiability_CreationDate = <cfqueryparam value="#arguments.liabilityDate#" cfsqltype="CF_SQL_DATE" />
			WHERE
				dbLiability_ID = <cfqueryparam value="#arguments.liabilityID#" cfsqltype="cf_sql_integer" />
			;
		</cfquery>

		<!--- Return --->
		<cfreturn 1/>
	</cffunction>

	<!--- Create_liability(liabilityName, liabilityParentFSID, liabilityDescription, liabilityEquity, liabilityDebt, liabilityDate): liability id--->
	<cffunction name="Create_liability" output="false" access= "public" returnType="string">
		<cfargument name="liabilityName" type="string" required="true" default="" />
		<cfargument name="liabilityParentFSID" type="string" required="false" default="" />
		<cfargument name="liabilityDescription" type="string" required="true" default="" />
		<cfargument name="liabilityEquity" type="string" required="false" default="" />
		<cfargument name="liabilityDebt" type="string" required="false" default="" />
		<cfargument name="liabilityDate" type="string" required="false" default="" />

		<cfset liabilityFSID = Create_FS(fsName = "#arguments.liabilityName#",fsParentID = "#arguments.liabilityParentFSID#")>

		<cfquery name="createliability" datasource="finfun_database">
			INSERT INTO dbLiability (dbLiability_Name, dbLiability_FinancialSt_ID ,dbLiability_ChildFS_ID , dbLiability_Description, dbLiability_Equity, dbLiability_Debt, dbLiability_Active, dbLiability_CreationDate) 
			VALUES (
				<cfqueryparam value="#arguments.liabilityName#" cfsqltype="CF_SQL_VARCHAR" />, 
				<cfqueryparam value="#arguments.liabilityParentFSID#" cfsqltype="cf_sql_integer" />,
				<cfqueryparam value="#liabilityFSID#" cfsqltype="cf_sql_integer" />, 
				<cfqueryparam value="#arguments.liabilityDescription#" cfsqltype="CF_SQL_VARCHAR" />,
				<cfqueryparam value="#arguments.liabilityEquity#" cfsqltype="CF_SQL_REAL" />, 
				<cfqueryparam value="#arguments.liabilityDebt#" cfsqltype="CF_SQL_REAL" />, 
				1,
				STR_TO_DATE(#DateFormat(arguments.liabilityDate , 'YYYY-MM-DD')#, '%Y-%m-%d')
			);
		</cfquery>

		<cfquery name="returnID" datasource="finfun_database" >
			SELECT LAST_INSERT_ID() AS ID;			
		</cfquery>

		<!--- Return --->
		<cfreturn #returnID.ID[1]#/>
	</cffunction>

	<!--- ##### Income Manager ##### --->
	<!--- Retrieve_income_data(incomeID): Query --->
	<cffunction name="Retrieve_income_data" output="false" access= "public" returnType="query">
		<cfargument name="incomeID" type="string" required="true" default="" />

		<cfquery name="incomeData" datasource="finfun_database" >
			SELECT * FROM dbIncome WHERE dbIncome_ID = 
				<cfqueryparam value="#arguments.incomeID#" cfsqltype="cf_sql_integer" />
			;			
		</cfquery>
		<cfreturn #incomeData#>
	</cffunction>

	<!--- Get_incomes(ParentFSID, requestType, From, To): query --->
	<cffunction name="Get_incomes" output="false" access= "public" returnType="query">
		<cfargument name="ParentFSID" type="string" required="true" default="" />
		<cfargument name="requestType" type="string" required="false" default="false" />
		<cfargument name="From" type="string" required="false" default="" />
		<cfargument name="To" type="string" required="false" default="" />

		<cfif #arguments.requestType#>
			<cfquery name="incomeData" datasource="finfun_database" >
				SELECT dbIncome_ID , dbIncome_Name, dbIncome_Amount, dbIncome_CreationDate FROM dbIncome WHERE 
					dbIncome_FinancialSt_ID = 
					<cfqueryparam value="#arguments.ParentFSID#" cfsqltype="cf_sql_integer" />
					<cfif IsDate(#arguments.From#) AND IsDate(#arguments.To#)>
					AND
					dbIncome_CreationDate BETWEEN 
					STR_TO_DATE('#arguments.From#', '%Y-%m-%d') 
					AND 
					STR_TO_DATE('#arguments.To#', '%Y-%m-%d')
					</cfif>
				;			
			</cfquery>
		<cfelse>
			<cfquery name="incomeData" datasource="finfun_database" >
				SELECT * FROM dbIncome WHERE 
					dbIncome_FinancialSt_ID = 
					<cfqueryparam value="#arguments.ParentFSID#" cfsqltype="cf_sql_integer" />
					<cfif IsDate(#arguments.From#) AND IsDate(#arguments.To#)>
					AND
					dbIncome_CreationDate BETWEEN 
					STR_TO_DATE('#arguments.From#', '%Y-%m-%d') 
					AND 
					STR_TO_DATE('#arguments.To#', '%Y-%m-%d')
					</cfif>
				;			
			</cfquery>
		</cfif>
		<cfreturn #incomeData#>
	</cffunction>

	<!---  Update_income(incomeID, incomeName, financialSt_ID, incomeDescription, incomeAmount, incomeDate): 1 --->
	<cffunction name="Update_income" output="false" access= "public" returnType="string">
		<cfargument name="incomeID" type="string" required="true" default="" />
		<cfargument name="incomeName" type="string" required="true" default="" />
		<cfargument name="financialSt_ID" type="string" required="false" default="" />
		<cfargument name="incomeDescription" type="string" required="true" default="" />
		<cfargument name="incomeAmount" type="string" required="true" default="" />
		<cfargument name="incomeDate" type="string" required="true" default="" />

		<cfquery name="createincome" datasource="finfun_database">
			UPDATE dbIncome SET
				<cfif len(financialSt_ID)>
					dbIncome_FinancialSt_ID = <cfqueryparam value="#arguments.financialSt_ID#" cfsqltype="cf_sql_integer" />,
				<cfelse>
				dbIncome_Name = <cfqueryparam value="#arguments.incomeName#" cfsqltype="CF_SQL_VARCHAR" />, 
				dbIncome_Description = <cfqueryparam value="#arguments.incomeDescription#" cfsqltype="CF_SQL_VARCHAR" />,
				dbIncome_Amount = <cfqueryparam value="#arguments.incomeAmount#" cfsqltype="CF_SQL_REAL" />,
				dbIncome_Category_ID = 0,
				dbIncome_CreationDate = <cfqueryparam value="#arguments.incomeDate#" cfsqltype="CF_SQL_DATE" />
				</cfif>
			WHERE
				dbIncome_ID = <cfqueryparam value="#arguments.incomeID#" cfsqltype="cf_sql_integer" />
			;
		</cfquery>

		<!--- Return --->
		<cfreturn 1/>
	</cffunction>

	<!---  Create_income(incomeName, financialSt_ID, incomeDescription, incomeAmount, incomeDate): incomeID --->
	<cffunction name="Create_income" output="false" access= "public" returnType="string">
		<cfargument name="incomeName" type="string" required="true" default="" />
		<cfargument name="financialSt_ID" type="string" required="true" default="" />
		<cfargument name="incomeDescription" type="string" required="true" default="" />
		<cfargument name="incomeAmount" type="string" required="true" default="" />
		<cfargument name="incomeDate" type="string" required="true" default="" />

		<cfquery name="createincome" datasource="finfun_database">
			INSERT INTO dbIncome (dbIncome_Name, dbIncome_FinancialSt_ID, dbIncome_Description, dbIncome_Amount, dbIncome_Category_ID , dbIncome_CreationDate)
			VALUES (
				<cfqueryparam value="#arguments.incomeName#" cfsqltype="CF_SQL_VARCHAR" />, 
				<cfqueryparam value="#arguments.financialSt_ID#" cfsqltype="cf_sql_integer" />, 
				<cfqueryparam value="#arguments.incomeDescription#" cfsqltype="CF_SQL_VARCHAR" />,
				<cfqueryparam value="#arguments.incomeAmount#" cfsqltype="CF_SQL_REAL" />,
				0,
				<cfqueryparam value="#arguments.incomeDate#" cfsqltype="CF_SQL_DATE" />
			);
		</cfquery>

		<cfquery name="returnID" datasource="finfun_database" >
			SELECT LAST_INSERT_ID() AS ID;			
		</cfquery>

		<!--- Return --->
		<cfreturn #returnID.ID[1]#/>
	</cffunction>

	<!--- Delete_income(incomeID): 1 --->
	<cffunction name="Delete_income" output="false" access= "public" returnType="string">
		<cfargument name="incomeID" type="string" required="true" default="" />

		<cfquery name="deleteincome" datasource="finfun_database">
			DELETE FROM dbIncome WHERE dbIncome_ID =
				<cfqueryparam value="#arguments.incomeID#" cfsqltype="cf_sql_integer" />
				LIMIT 1
			;			
		</cfquery>

		<!--- Return --->
		<cfreturn 1/>
	</cffunction>
	
	<!--- ##### Expense Manager ##### --->
	<!--- Retrieve_expense_data(expenseID): Query --->
	<cffunction name="Retrieve_expense_data" output="false" access= "public" returnType="query">
		<cfargument name="expenseID" type="string" required="true" default="" />

		<cfquery name="expenseData" datasource="finfun_database" >
			SELECT * FROM dbExpense WHERE dbExpense_ID = 
				<cfqueryparam value="#arguments.expenseID#" cfsqltype="cf_sql_integer" />
			;			
		</cfquery>
		<cfreturn #expenseData#>
	</cffunction>

	<!--- Get_expenses(ParentFSID, requestType, From, To): query --->
	<cffunction name="Get_expenses" output="false" access= "public" returnType="query">
		<cfargument name="ParentFSID" type="string" required="true" default="" />
		<cfargument name="requestType" type="string" required="false" default="false" />
		<cfargument name="From" type="string" required="false" default="" />
		<cfargument name="To" type="string" required="false" default="" />

		<cfif #arguments.requestType#>
			<cfquery name="expenseData" datasource="finfun_database" >
				SELECT dbExpense_ID, dbExpense_Name, dbExpense_Amount, dbExpense_CreationDate FROM dbExpense WHERE dbExpense_FinancialSt_ID = 
					<cfqueryparam value="#arguments.ParentFSID#" cfsqltype="cf_sql_integer" />
					<cfif IsDate(#arguments.From#) AND IsDate(#arguments.To#)>
					AND
					dbExpense_CreationDate BETWEEN 
					STR_TO_DATE('#arguments.From#', '%Y-%m-%d') 
					AND 
					STR_TO_DATE('#arguments.To#', '%Y-%m-%d')
					</cfif>
				;			
			</cfquery>
		<cfelse>
			<cfquery name="expenseData" datasource="finfun_database" >
				SELECT * FROM dbExpense WHERE dbExpense_FinancialSt_ID = 
					<cfqueryparam value="#arguments.ParentFSID#" cfsqltype="cf_sql_integer" />
					<cfif IsDate(#arguments.From#) AND IsDate(#arguments.To#)>
					AND
					dbExpense_CreationDate BETWEEN 
					STR_TO_DATE('#arguments.From#', '%Y-%m-%d') 
					AND 
					STR_TO_DATE('#arguments.To#', '%Y-%m-%d')
					</cfif>
				;			
			</cfquery>
		</cfif>
		<cfreturn #expenseData#>
	</cffunction>

	<!---  Update_expense(expenseID, expenseName, financialSt_ID, expenseDescription, expenseAmount, expenseDate): 1 --->
	<cffunction name="Update_expense" output="false" access= "public" returnType="string">
		<cfargument name="expenseID" type="string" required="true" default="" />
		<cfargument name="expenseName" type="string" required="true" default="" />
		<cfargument name="financialSt_ID" type="string" required="false" default="" />
		<cfargument name="expenseDescription" type="string" required="true" default="" />
		<cfargument name="expenseAmount" type="string" required="true" default="" />
		<cfargument name="expenseDate" type="string" required="true" default="" />

		<cfquery name="createexpense" datasource="finfun_database">
			UPDATE dbExpense SET
				<cfif len(financialSt_ID)>
					dbExpense_FinancialSt_ID = <cfqueryparam value="#arguments.financialSt_ID#" cfsqltype="cf_sql_integer" />,
				<cfelse>
				dbExpense_Name = <cfqueryparam value="#arguments.expenseName#" cfsqltype="CF_SQL_VARCHAR" />, 
				dbExpense_Description = <cfqueryparam value="#arguments.expenseDescription#" cfsqltype="CF_SQL_VARCHAR" />,
				dbExpense_Amount = <cfqueryparam value="#arguments.expenseAmount#" cfsqltype="CF_SQL_REAL" />,
				dbExpense_Category_ID = 0,
				dbExpense_CreationDate = <cfqueryparam value="#arguments.expenseDate#" cfsqltype="CF_SQL_DATE" />
				</cfif>
			WHERE
				dbExpense_ID = <cfqueryparam value="#arguments.expenseID#" cfsqltype="cf_sql_integer" />
			;
		</cfquery>

		<!--- Return --->
		<cfreturn 1/>
	</cffunction>

	<!---  Create_expense(expenseName, financialSt_ID, expenseDescription, expenseAmount, expenseDate): expenseID --->
	<cffunction name="Create_expense" output="false" access= "public" returnType="string">
		<cfargument name="expenseName" type="string" required="true" default="" />
		<cfargument name="financialSt_ID" type="string" required="true" default="" />
		<cfargument name="expenseDescription" type="string" required="false" default="" />
		<cfargument name="expenseAmount" type="string" required="true" default="" />
		<cfargument name="expenseDate" type="string" required="false" default="" />

		<cfquery name="createexpense" datasource="finfun_database">
			INSERT INTO dbExpense (dbExpense_Name, dbExpense_FinancialSt_ID, dbExpense_Description, dbExpense_Amount, dbExpense_Category_ID, 
					   dbExpense_CreationDate )
			VALUES (
				<cfqueryparam value="#arguments.expenseName#" cfsqltype="CF_SQL_VARCHAR" />, 
				<cfqueryparam value="#arguments.financialSt_ID#" cfsqltype="cf_sql_integer" />, 
				<cfqueryparam value="#arguments.expenseDescription#" cfsqltype="CF_SQL_VARCHAR" />,
				<cfqueryparam value="#arguments.expenseAmount#" cfsqltype="CF_SQL_REAL" />,
				0,
				<cfqueryparam value="#arguments.expenseDate#" cfsqltype="CF_SQL_DATE" />
			);
		</cfquery>

		<cfquery name="returnID" datasource="finfun_database" >
			SELECT LAST_INSERT_ID() AS ID;			
		</cfquery>

		<!--- Return --->
		<cfreturn #returnID.ID[1]#/>
	</cffunction>

	<!--- Delete_expense(expenseID): 1 --->
	<cffunction name="Delete_expense" output="false" access= "public" returnType="string">
		<cfargument name="expenseID" type="string" required="true" default="" />

		<cfquery name="deleteexpense" datasource="finfun_database">
			DELETE FROM dbExpense WHERE dbExpense_ID =
				<cfqueryparam value="#arguments.expenseID#" cfsqltype="cf_sql_integer" />
				LIMIT 1
			;			
		</cfquery>

		<!--- Return --->
		<cfreturn 1/>
	</cffunction>


</cfcomponent>