<!--- Income_set Object --->
<cfcomponent output="false">
    <cfset variables.expensesQuery = 0>

    <cffunction name="init" output="false">
        <cfargument name="expensesQuery" required="true">
        <cfset variables.expensesQuery = arguments.expensesQuery>
        <cfquery name="result" dbtype="query">
            SELECT * FROM variables.expensesQuery order by DBEXPENSE_AMOUNT desc
        </cfquery>
        <cfset variables.expensesQuery = result>
        <cfreturn this>
    </cffunction>

    <cffunction name="print" output="true" access= "public" returnType="void">
		<cfdump var = #variables.expensesQuery#>	
	</cffunction>

	<cffunction name="top" output="true" access= "public" returnType="query">
		<cfreturn #variables.expensesQuery#>	
	</cffunction>

	<cffunction name="mean" output="false" access= "public" returnType="string">
        <cfquery name="result" dbtype="query">
            SELECT AVG(DBEXPENSE_AMOUNT) AS result FROM variables.expensesQuery
        </cfquery>
        <cfreturn #result.result#> 	
	</cffunction>

	<cffunction name="sum" output="false" access= "public" returnType="string">
		<cfquery name="result" dbtype="query">
            SELECT SUM(DBEXPENSE_AMOUNT) AS result FROM variables.expensesQuery
        </cfquery>
        <cfreturn #result.result#>	
	</cffunction>

	<cffunction name="count" output="false" access= "public" returnType="string">
		<cfreturn #variables.expensesQuery.RecordCount#>	
	</cffunction>

	<cffunction name="max" output="false" access= "public" returnType="string">
		<cfset maxIncomeID= #variables.expensesQuery.DBEXPENSE_AMOUNT[1]#>
        <cfreturn maxIncomeID>	
	</cffunction>

	<cffunction name="min" output="false" access= "public" returnType="string">
		<cfset maxIncomeID= #variables.expensesQuery.DBEXPENSE_AMOUNT[#variables.expensesQuery.RecordCount#]#>
        <cfreturn maxIncomeID>		
	</cffunction>

	<cffunction name="std" output="false" access= "public" returnType="string">
		<cfset myList = ValueList(variables.expensesQuery.DBEXPENSE_AMOUNT)>
        <cfreturn #StdDevPop(myList)#>	
	</cffunction>

	<cffunction name="var" output="false" access= "public" returnType="string">
		<cfreturn #std()# * #std()#>	
	</cffunction>

	<cffunction name="median" output="false" access= "public" returnType="string">	
		<cfset myList = ValueList(variables.expensesQuery.DBEXPENSE_AMOUNT)>
		<cfreturn #Median_private(myList)#>	
	</cffunction>
	
	<cffunction name="range" output="false" access= "public" returnType="string">
        <cfreturn #variables.expensesQuery.DBEXPENSE_AMOUNT[1]# - #variables.expensesQuery.DBEXPENSE_AMOUNT[#variables.expensesQuery.RecordCount#]#>	
	</cffunction>

	<cffunction name="data_as_array" output="false" access= "public" returnType="array">
		<cfquery name="result" dbtype="query">
            SELECT * FROM variables.expensesQuery order by DBEXPENSE_CREATIONDATE asc;
        </cfquery>
		<!--- Declare the array ---> 
		<cfset myarray=arraynew(2)> 
		 
		<!--- Populate the array row by row ---> 
		<cfloop query="result"> 
		    <cfset myarray[CurrentRow][1]=DBEXPENSE_NAME> 
		    <cfset myarray[CurrentRow][2]=DBEXPENSE_AMOUNT> 
		    <cfset myarray[CurrentRow][3]=DBEXPENSE_CREATIONDATE> 
		</cfloop>
		<cfreturn #myarray#>
	</cffunction>

	<cffunction name="data_as_string" output="true" access= "public" returnType="void">
		<cfset array_data = data_as_array()>
		<cfset counter_k = 1>
		<cfloop index='row' array=#array_data#>
			<cfif counter_k EQ 1>
				<cfoutput>[['Incomes', 'Amount'],['#row[1]#', #row[2]#],</cfoutput>
			<cfelseif  counter_k NEQ #ArrayLen(array_data)#>
				<cfoutput>['#row[1]#', #row[2]#],</cfoutput>
			<cfelse>
				<cfoutput>['#row[1]#', #row[2]#]]</cfoutput>
			</cfif>
			<cfset counter_k = counter_k + 1>
		</cfloop>
	</cffunction>

	<cffunction name="acum_data_as_array" output="true" access= "public" returnType="array">
		<cfquery name="result" dbtype="query">
            SELECT * FROM variables.expensesQuery order by DBEXPENSE_CREATIONDATE asc;
        </cfquery>
		<!--- Declare the array ---> 
		<cfset myarray=arraynew(2)> 
		<cfset acum = 0>
		<cfset offset = 0>
		<!--- Populate the array row by row ---> 
		<cfloop query="result"> 
			<cfset acum = acum + DBEXPENSE_AMOUNT>
			<cfif CurrentRow - 1><cfif DBEXPENSE_CREATIONDATE EQ myarray[CurrentRow - 1 - offset][2]>
				<cfset offset = offset + 1>
			</cfif></cfif>

		    <cfset myarray[CurrentRow - offset][1]=acum> 
		    <cfset myarray[CurrentRow - offset][2]=DBEXPENSE_CREATIONDATE> 
		</cfloop>
		<cfreturn #myarray#>
	</cffunction>


	<cfinclude template="Math_func.cfm">
</cfcomponent>