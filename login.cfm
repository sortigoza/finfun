<cfquery name="login" datasource="finfun_database">
    SELECT * FROM dbUser WHERE dbUser_UserName = '#FORM.userName#' AND dbUser_Password = SHA1('#FORM.pswd#')
</cfquery>

<cfif login.RecordCount>
    <!--- This user has logged in correctly, change the value of the session.user.allowin value --->
    <cfset session.user.allowin = "True">
    <cfset session.user.userID = login.dbUser_ID>
    <cfset session.user.userClass = login.dbUser_Class>
    <cfset session.user.userName = login.dbUser_Name>
    <cfif session.user.userClass EQ 1>
        <cfquery name="rootFS" datasource="finfun_database">
            SELECT * FROM dbUser_FinancialSt_link WHERE dbUser_FinancialSt_link_User_ID = #login.dbUser_ID#
        </cfquery>
        <cfset session.user.userRootFSID = rootFS.dbUser_FinancialSt_link_RootFS_ID>
        <cfset session.user.userCurrentFSID = rootFS.dbUser_FinancialSt_link_RootFS_ID>
        <cfset session.user.userCurrentFilter = [0, 0, 0]>
    </cfif>
    <cfset session.user.userTempVar1 = 0>
    <cfset session.user.userTempVar2 = 0>
    <cfset session.user.userTempVar3 = 0>
    <cfset session.user.sessionid = RandRange(1000, 9999, "SHA1PRNG")>
    <!--- Now welcome user and redirect to "members_only.cfm" --->
	1
<cfelse>
    <!--- this user did not log in correctly, alert and redirect to the login page --->
    0
</cfif>