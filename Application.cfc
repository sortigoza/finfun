<cfcomponent>
    <cffunction name="OnRequestStart">
        <!--- Login --->
        <cfapplication 
          name="myLogin" 
          clientmanagement="Yes"
          sessionmanagement="Yes"
          sessiontimeout="#CreateTimeSpan(0,30,0,0)#" 
          applicationtimeout="#CreateTimeSpan(1,0,0,0)#">
        
        <!--- Define that this user is logged out by default --->
        <CFPARAM NAME="session.user.allowin" DEFAULT="false">
        
        <!--- Define this user id to zero by default,
        this will be used later on to access specific information about this user. --->
        <CFPARAM NAME="session.user.userID" DEFAULT="0">
        <CFPARAM NAME="session.user.userClass" DEFAULT="0">
        <CFPARAM NAME="session.user.userName" DEFAULT="none">
          
        <!--- Now if the variable "session.allowin" does not equal true, send user to the login page --->
        <!---the other thing you must check for is if the page calling this application.cfm is the "default.cfm" page 
                and the "login.cfm" page since the Application.cfm is always called, if this is not checked 
                the application will simply Loop over and over. To check that, you do the following call --->
        <cfif session.user.allowin neq "true">
              <cfif ListLast(CGI.SCRIPT_NAME, "/") EQ "default.cfm">
              <cfelseif ListLast(CGI.SCRIPT_NAME, "/") EQ "login.cfm">
              <cfelseif ListLast(CGI.SCRIPT_NAME, "/") EQ "timeout.cfm">
              <cfelseif ListLast(CGI.SCRIPT_NAME, "/") EQ "test.cfm">
              <cfelse>
        <!--- this user is not logged in, alert user and redirect to the default.cfm page --->
                <cflocation url="default.cfm">
              </cfif>
        </cfif>
    </cffunction>
</cfcomponent>

