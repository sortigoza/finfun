<cfset finman = CreateObject("cfc\financialStManager") />
<cfset finfunc = CreateObject("cfc\financialStFunctions") />
<cfset getincomes = finman.Get_incomes(#session.user.userCurrentFSID#)>
<cfset getassets = finman.Get_assets(#session.user.userCurrentFSID#)>

<cfdump var = #getincomes#>
<cfdump var = #getassets#>

< !--- FS Functions --- ><br>
Get_FS_All_Depth
<cfset fs = finfunc.Get_FS_All_Depth(#session.user.userCurrentFSID#)/>
<cfdump var = #fs#>

< !--- Incomes Functions --- ><br>
Get_Incomes_All_Depth
<cfset fs = finfunc.Get_Incomes_All_Depth(#session.user.userCurrentFSID#)/>
<cfdump var = #fs#>

Get_TotalIncome_All_Depth
<cfset fs = finfunc.Get_TotalIncome_All_Depth(#session.user.userCurrentFSID#)/>
<cfdump var = #fs#><br>

< !--- Expenses Functions --- ><br>
Get_Expenses_All_Depth
<cfset fs = finfunc.Get_Expenses_All_Depth(#session.user.userCurrentFSID#)/>
<cfdump var = #fs#><br>

Get_TotalExpense_All_Depth
<cfset fs = finfunc.Get_TotalExpense_All_Depth(#session.user.userCurrentFSID#)/>
<cfdump var = #fs#><br>

< !--- Net Income Functions --- ><br>
Get_NetIncome_All_Depth
<cfset fs = finfunc.Get_NetIncome_All_Depth(#session.user.userCurrentFSID#)/>
<cfdump var = #fs#><br>

< !--- Assets Functions --- ><br>

Get_Assets_All_Depth
<cfset fs = finfunc.Get_Assets_All_Depth(#session.user.userCurrentFSID#)/>
<cfdump var = #fs#><br>

Get_Assets_E_D_All_Depth
<cfset fs = finfunc.Get_Assets_E_D_All_Depth(#session.user.userCurrentFSID#)/>
<cfdump var = #fs#><br>

< !--- Liabilities Functions --- ><br>

Get_Liabilities_All_Depth
<cfset fs = finfunc.Get_Liabilities_All_Depth(#session.user.userCurrentFSID#)/>
<cfdump var = #fs#><br>

Get_Liabilities_E_D_All_Depth
<cfset fs = finfunc.Get_Liabilities_E_D_All_Depth(#session.user.userCurrentFSID#)/>
<cfdump var = #fs#><br>
