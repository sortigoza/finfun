<cfset finman = CreateObject("cfc\financialStManager") />

++++Function Test: Get_Incomes++++<br>

-No Arguments<br>
Shows all...
<cfset incomes = finman.Get_incomes(#session.user.userRootFSID#)>
<cfdump var = #incomes#>

-requestType 0 and 1 <br>
Shows all...
<cfset incomes = finman.Get_incomes(#session.user.userRootFSID#, 0)>
<cfdump var = #incomes#>
Shows fewer data...
<cfset incomes = finman.Get_incomes(#session.user.userRootFSID#, 1)>
<cfdump var = #incomes#>

-Dates filter <br>
Shows from Dates: '2014-08-1', '2014-08-30' with all data...
<cfset incomes = finman.Get_incomes(#session.user.userRootFSID#,0, '2014-08-1', '2014-08-30')>
<cfdump var = #incomes#>

Shows from Dates: '2014-09-1', '2014-09-30' with fewer data...
<cfset incomes = finman.Get_incomes(#session.user.userRootFSID#,1, '2014-09-1', '2014-09-30')>
<cfdump var = #incomes#>

++++Function Test: Create income++++<br>

<cfset incomesID = finman.Create_income('Test Create Income', 1, 'Test Create Income incomeDescription', 1000.00, '11-09-2014')>
ID: <cfdump var = #incomesID#><br>

++++Function Test: Retrieve income++++<br>

-All Arguments<br>
<cfset incomes = finman.Retrieve_income_data(#incomesID#)>
<cfdump var = #incomes#><br>

++++Function Test: Update income++++<br>

-All Arguments<br>
<cfset incomes = finman.Update_income(incomeID = #incomesID#, incomeName = 'form.INCOMENAME', incomeDescription = 'form.INCOMEDESCRIPTION', incomeAmount = 2000, incomeDate = '11-09-2014')>
<cfdump var = #incomes#><br>

++++Function Test: Retrieve income++++<br>

-All Arguments<br>
<cfset incomes = finman.Retrieve_income_data(#incomesID#)>
<cfdump var = #incomes#><br>

++++Function Test: Delete income++++<br>
<cfset incomesID = finman.Delete_income(#incomesID#)>
Delete OK<br>
	


++++Function Test: Get_Expenses++++<br>

-No Arguments<br>
Shows all...
<cfset expenses = finman.Get_expenses(#session.user.userRootFSID#)>
<cfdump var = #expenses#>

-requestType 0 and 1 <br>
Shows all...
<cfset expenses = finman.Get_expenses(#session.user.userRootFSID#, 0)>
<cfdump var = #expenses#>
Shows fewer data...
<cfset expenses = finman.Get_expenses(#session.user.userRootFSID#, 1)>
<cfdump var = #expenses#>

-Dates filter <br>
Shows from Dates: '2014-08-1', '2014-08-30' with all data...
<cfset expenses = finman.Get_expenses(#session.user.userRootFSID#,0, '2014-08-1', '2014-08-30')>
<cfdump var = #expenses#>

Shows from Dates: '2014-09-1', '2014-09-30' with fewer data...
<cfset expenses = finman.Get_expenses(#session.user.userRootFSID#,1, '2014-09-1', '2014-09-30')>
<cfdump var = #expenses#>


++++Function Test: Create expense++++<br>

<cfset expensesID = finman.Create_expense('Test Create Expense', 1, 'Test Create Expense expenseDescription', 1000.00, '11-09-2014')>
ID: <cfdump var = #expensesID#><br>

++++Function Test: Retrieve expense++++<br>

-All Arguments<br>
<cfset expenses = finman.Retrieve_expense_data(#expensesID#)>
<cfdump var = #expenses#><br>

++++Function Test: Update expense++++<br>

-All Arguments<br>
<cfset expenses = finman.Update_expense(expenseID = #expensesID#, expenseName = 'form.expenseNAME', expenseDescription = 'form.expenseDESCRIPTION', expenseAmount = 2000, expenseDate = '11-09-2014')>
<cfdump var = #expenses#><br>

++++Function Test: Retrieve expense++++<br>

-All Arguments<br>
<cfset expenses = finman.Retrieve_expense_data(#expensesID#)>
<cfdump var = #expenses#><br>


++++Function Test: Delete expense++++<br>
<cfset expensesID = finman.Delete_expense(#expensesID#)>
Delete OK<br>



++++Function Test: Get_Assets++++<br>

-No Arguments<br>
Shows all...
<cfset assets = finman.Get_Assets(#session.user.userRootFSID#)>
<cfdump var = #assets#>

++++Function Test: Create asset++++<br>
Create_asset(assetName, assetParentFSID, assetDescription, assetEquity, assetDebt, assetDate)
<cfset assetID = finman.Create_Asset(assetName = 'Test Create Asset', assetParentFSID = 1, assetDescription=  'Test Create asset expenseDescription',assetEquity =  1000.00, assetDebt = 1000.00, assetDate = '11-09-2014')>
ID: <cfdump var = #assetID#><br>

++++Function Test: Retrieve asset++++<br>

-All Arguments<br>
<cfset assets = finman.Retrieve_asset_data(#assetID#)>
<cfdump var = #assets#><br>


++++Function Test: Update asset++++<br>
Update_assset(asssetID, asssetName, financialSt_ID, asssetDescription, asssetDebt, asssetDate)
-All Arguments<br>
<cfset assets = finman.Update_asset(assetID = #assetID#, assetName = 'form.assetNAME', assetDescription = 'form.assetDESCRIPTION', assetEquity = 2000, assetDebt = 2000, assetDate = '11-09-2014')>
<cfdump var = #assets#><br>

++++Function Test: Retrieve asset++++<br>

-All Arguments<br>
<cfset assets = finman.Retrieve_asset_data(#assetID#)>
<cfdump var = #assets#><br>

++++Function Test: Delete asset++++<br>
<cfset assetID = finman.Delete_asset(#assetID#)>
Delete OK<br>