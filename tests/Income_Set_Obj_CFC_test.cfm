<cfset finfunc = CreateObject("cfc\financialStFunctions") />
<cfset finman = CreateObject("cfc\financialStManager") />
<cfset incomes = finman.Get_incomes(#session.user.userRootFSID#)>
<cfset income_set = CreateObject("cfc\Income_Set_obj").init(#incomes#) />

++++Function Test: print++++<br>
<cfset test = income_set.print() />

++++Function Test: mean++++<br>
<cfset test = income_set.mean() />
<cfdump var = #test#><br>

++++Function Test: sum++++<br>
<cfset test = income_set.sum() />
<cfdump var = #test#><br>

++++Function Test: count++++<br>
<cfset test = income_set.count() />
<cfdump var = #test#><br>

++++Function Test: max++++<br>
<cfset test = income_set.max() />
<cfdump var = #test#><br>

++++Function Test: min++++<br>
<cfset test = income_set.min() />
<cfdump var = #test#><br>

++++Function Test: std++++<br>
<cfset test = income_set.std() />
<cfdump var = #test#><br>

++++Function Test: acum_data_as_array++++<br>
<cfset test = income_set.acum_data_as_array() />
<cfdump var = #test#><br>

<cfset expenses = finfunc.Get_Expenses_All_Depth(#session.user.userRootFSID#)>
<cfset expense_set = CreateObject("cfc\Expense_Set_obj").init(#expenses#) />

++++Function Test: print++++<br>
<cfset test = expense_set.print() />

++++Function Test: mean++++<br>
<cfset test = expense_set.mean() />
<cfdump var = #test#><br>

++++Function Test: sum++++<br>
<cfset test = expense_set.sum() />
<cfdump var = #test#><br>

++++Function Test: count++++<br>
<cfset test = expense_set.count() />
<cfdump var = #test#><br>

++++Function Test: max++++<br>
<cfset test = expense_set.max() />
<cfdump var = #test#><br>

++++Function Test: min++++<br>
<cfset test = expense_set.min() />
<cfdump var = #test#><br>

++++Function Test: std++++<br>
<cfset test = expense_set.std() />
<cfdump var = #test#><br>

++++Function Test: acum_data_as_array++++<br>
<cfset test = expense_set.acum_data_as_array() />
<cfdump var = #test#><br>
