<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfif session.user.allowin>
  <cflocation url="admin/app/index.cfm">
<cfelse>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--- <meta http-equiv="X-UA-Compatible" content="IE=edge">--->

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="keywords" content="Personal finances, finances, financial tool">
    <meta name="description" content="personal finances tool">
    <meta name="author" content="Saul Ortigoza">
    <link rel="icon" href="/assets/ico/Dollar.ico" type="image/x-icon">

    <title>Fin-Fun</title>

    <!--- CSS --->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/defaultpage.css" rel="stylesheet" type="text/css">

    <!--- JavaScript --->
    <script type="text/javascript" src="javascript/jquery.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.js"></script>

  </head>

  <!--- Body Starts --->
  <body>
    <cfinclude template="/admin/presentation/main_default_view.cfm">
  </body>
</html>
</cfif>

