<cfprocessingdirective pageencoding="utf-8">
<!--- Barra de Navegacion --->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Fin-Fun</a>
        </div>
        <div class="navbar-collapse collapse">
          <!--- Nav Buttons --->
          
          <!--- User log-in Form --->
          <form class="navbar-form navbar-right" role="form" name="logMeIn" id="logMeIn">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control"  id="userName" name="userName">
            </div>
            <div class="form-group">
              <input id="pswd" name="pswd" type="password" class="form-control" placeholder="Password">
            </div>
            <button id="startLogIn" type="submit" class="btn btn-default">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" id="defaulJumbotron">
        <div class="container">
          <h1>FIN-FUN</h1>
          <p>Manage your personal finances in your own way.</p>
          <!---
          <p><a class="btn btn-default" href="#" role="button">SIGN UP NOW <span class="glyphicon glyphicon-chevron-right"></span></a></p>
          --->
        </div>
      </div>

      <div class="container" id="defaultPageContainer">
        <hr>
        <!-- Example row of columns -->
        <div class="row">
          <div class="col-sm-12"><h1 class="center">Nuestra Misión y Visión</h1></div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <h2 class="center">La Misión</h2>
            <p>Nuestra misión es contribuir a la educación de la gente en cuestión de educación financiera. proporcionar herramientas que ayuden a conocer y administrar sus finanzas personales (estado financiero). y por ultimo, asesorar y evaluar su desempeño y salud financiera brindando seguridad y proporcionando guia. Finfun se centra en crear utilidades que agregen valor a las finanzas personales de las personas en México y en el mundo.</p>
            <p>A nuestros clientes les proporcionamos un puente entre clientes potenciales y ellos atravéz de Finfun además de agregar valor a Funfun, ofreciendo a nuestros usuarios los productos de nuestros clientes.</p>
          </div>
          <div class="col-sm-4">
            <h2 class="center">La Visión</h2>
            <p>En un futuro no muy lejano, podemos ver el impacto que Finfun a creado en la sociedad mexicana y usuarios sobre su educación financiera y el manejo de sus finanzas personales, ahora que cuentan con la ayuda de Finfun estan menos inciertos de lo que les espera el futuro y lo que sucede con el dinero. Nuestros usuarios nos ven como la mejor aplicacion para el manejo de finanzas personales además de tener una gran confianza en nosotros.  Somos recomendados por nuestros mismos usuarios y tenemos un gran numero de seguidores.</p>
            <p>Nuestros clientes nos ven como una buena inversión y obtienen buenos resultados de la relación que tienen con Finfun, esto se ve en el aumento de visitas a sus paginas, son conocidos por nuestros usuarios y tienen mas ganancias.</p>
          </div>
          <div class="col-sm-4">
            <h2 class="center">Valores</h2>
            <ul>
              <li>Proporcionar valor al usuario y al cliente es lo mas importante.</li>
              <li>Perfección - Simpleza, Cuidar el Detalle, Sin errores, Sin defectos.</li>
              <li>Precisión - Exactitud, Alta calidad, todo y cada parte tiene un propósito. reducir desperdicios.</li>
              <li>Belleza - Estético, Inspiración de la naturaleza(expresión de Dios) y el arte(sentimientos y emociones), Equilibrio y Armonía.</li>
              <li>Formalidad - Utilizando Estándares, Clara Documentación, Siguiendo Procesos, uso de la Ley y el Orden.</li>
              <li>Filosofia LEAN y 5S.</li>
            </ul>
          </div>
        </div><!-- /row -->
        <hr>
        <div class="row">
          <div class="col-sm-12"><h1 class="center">Objetivos</h1></div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <h2 class="center">Objetivos Estratégicos</h2>
            <p>Resultados esperados dentro de un plazo de cinco años. (09/2014 - 09/2019)</p>
            <ul>
              <li>Tenemos mas de 5000 usuarios activos que además piensan que Finfun es la mejor aplicación para manejo de finanzas personales.</li>
              <li>Estos 5000 usuarios, han mejorado la salud de su estado financiero y tienen una mayor comprensión de como funciona el dinero.</li>
              <li>Tenemos mas de 50 clientes que ofrecen sus servición a travéz de finfun y desean seguir trabajando con nosotros.</li>
            </ul>
          </div>
          <div class="col-sm-4">
            <h2 class="center">Objetivos Tácticos</h2>
            <p>Resultados esperados dentro de un plazo de un año. (09/2014 - 09/2015)</p>
            <ul>
              <li>Tenemos 100 usuarios activos.</li>
              <li>tenemos 10 clientes activos</li>
            </ul>
          </div>
          <div class="col-sm-4">
            <h2 class="center">Objetivos Operativos</h2>
            <p>Resultados esperados dentro de un plazo de tres meses. (09/2014 - 01/2015)</p>
            <ul>
              <li>Tener el dominio y hosting para Finfun listo.</li>
              <li>Hacer el lanzamiento de la pimera versión, versión beta, de Finfun.</li>
              <li>Reunir retroalimentación, arreglar bugs y empezar la siguiente versión.</li>
              <li>Empezar un plan de marketing.</li>
            </ul>
          </div>
        </div><!-- /row -->
        <div class="row">
          <div class="col-sm-12">
            <p>*Objetivos - Establecen lo qué se debe realizar a partir de una situación presente para llegar a una situación futura y proponen los recursos y medios con los que se cuenta para lograrlo.</p>
          </div>
        </div><!-- /row -->

        <hr>
      </div> <!-- /container -->

      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12"><h1 class="center">Modelo de Negocio</h1></div>
        </div>
        <div class="row">
          <div class="col-sm-1"></div>
          <div class="col-sm-2 panel panel-default"><h3 class="BMheading">Key Partners</h3></div>
          <div class="col-sm-2">
            <div class="row">
              <div class="col-sm-12 panel panel-default"><h3 class="BMheading BMheading-small" style="min-height: 52px;">Key Activities</h3></div>
            </div>
            <div class="row">
              <div class="col-sm-12 panel panel-default"><h3 class="BMheading BMheading-small">Key Resources</h3></div>
            </div>
          </div>
          <div class="col-sm-2 panel panel-default"><h3 class="BMheading">Value Propositions</h3></div>
          <div class="col-sm-2">
            <div class="row">
              <div class="col-sm-12 panel panel-default"><h3 class="BMheading BMheading-small">Customer Relationship</h3></div>
            </div>
            <div class="row">
              <div class="col-sm-12 panel panel-default"><h3 class="BMheading BMheading-small">Channels</h3></div>
            </div>
          </div>
          <div class="col-sm-2 panel panel-default panel"><h3 class="BMheading">Customer Segments</h3></div>
          <div class="col-sm-1"></div>
        </div>
        <div class="row">
          <div class="col-sm-1"></div>
          <div class="col-sm-5 panel panel-default"><h3 class="BMheading">Cost Structure</h3></div>
          <div class="col-sm-5 panel panel-default panel"><h3 class="BMheading">Revenue Stream</h3></div>
          <div class="col-sm-1"></div>
        </div>
      </div>

<footer>
      <div class="container" style='line-height: 80px;'>
        &copy; Fin-Fun Company 2014
      </div>
</footer><!--/.footer -->

<script type="text/javascript">
$(function(){
  $('#userName').focus();
})

$(document).ready(function(){
  
  function validateLogin(formData){
    $.ajax({
      url: 'login.cfm',
      data: formData,
      type: 'post',
      success: function(data){
        if( data == 1 ){
          document.location.href = 'admin/app/index.cfm'; //relative to domain
        } else {
          //$('#defaultPageContainer').html(data);
          if ($('.failLogIn').show()){
            $('.failLogIn .shakeMe').effect("shake", { times:2, distance:10 }, 40);
          }
          $('.failLogIn').show();
        }
      }   
    });
  }

  $('#startLogIn').click(function(e){
    e.preventDefault();
    var okgo = 0;
    var totalInputs = 0;

    $('#logMeIn input').each(function(index){
      if( $(this).val() == '' ){
        $(this).effect("shake", { times:2, distance:10 }, 40);
      } else {
        okgo = okgo + 1;
      }
      totalInputs = index + 1;
    });
    if (okgo == totalInputs){
      validateLogin($('#logMeIn').serialize());
    }
  });
  
  window.onbeforeunload = function () {
     // This fucntion does nothing.  It won't spawn a confirmation dialog
     // But it will ensure that the page is not cached by the browser.
  }
  
  $('#userName, #pswd').keypress(function(key) {
    if (key.which == 13) {
    $('#startLogIn').click();
    }
  });
  
  $('#logMeIn').submit(function(Event) {
    Event.preventDefault();
    Event.stopPropagation();
      //$('#startLogIn').click();
  });
  
});
</script>