<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - <text>
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 31/05/2014
////////////////////////////////////////////////////// --->

<!--- Barra de Navegacion --->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Fin-Fun</a>
        </div>
        <div class="navbar-collapse collapse">
          <!--- Nav Buttons --->
          <ul class="nav navbar-nav" id="navButtons">
            <li class="active"><a href="#financialstatement" id="myFinancialStatementLink">My Financial Statement</a></li>
            <li><a href="#statisticsreports" id="statisticsReportsLink">Statistics/Reports</a></li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 0px;"><cfoutput>&nbsp;#session.user.userName#&nbsp;</cfoutput><span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
                <li class="divider"></li>
                <li><a href="#" id="logoutButton"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
              </ul>
            </li>
          </ul>
          <p class="navbar-text navbar-right" style="line-height: 20px;">Signed in as</p>
          
        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <!--- Filter nav bar --->
    <div class="navbar navbar-inverse" id="modeBar_navBar">
      <cfinclude template="financialStatement/PL_modeBar.cfm">
    </div>

    <!-- Main Container -->
    <div id="mainContainer" class="container">
      <div id="blinde" style="display: none">
        <div id="blindeMainContent"></div>
        <div id="closeForm"><span class="glyphicon glyphicon-remove"></span> Close</div>
      </div><!--/.blinde -->

      <div id="modalBox"></div>

      <div class="row" id="mainRowContainer">

        <!--- Tips Container --->
        <div class="col-md-2" id="leftColumn">
          <cfinclude template="finFun_leftColumn/leftColumn_index.cfm">
        </div><!--/.Tips Container -->

        <!--- Main Central Container --->
        <div class="col-md-8" id="mainContentContainer">
          <cfinclude template="financialStatement/financialStatement_index.cfm">
        </div><!--/.Main Central Container -->
        
        <!--- Adds Container --->
        <div class="col-md-2" id="adsColumn">
           <cfinclude template="funFun_adds/addsColumn_index.cfm">
        </div>
      </div><!--/.Adds Container -->
    </div><!--/.Main Container -->

    <footer>
      <div class="container" style='line-height: 80px;'>
        &copy; Fin-Fun Company 2014
      </div>
    </footer><!--/.footer -->






<script type="text/javascript">
    $('#myFinancialStatementLink').click(function(e){
        e.preventDefault();
        $(this).parent().addClass('active');
        $('#statisticsReportsLink').parent().removeClass('active');
        $('#navBar').show(200);

        $.post( "financialStatement/financialStatement_Index.cfm", function( data ) { $( "#mainContentContainer" ).html( data ); });
    });
    $('#statisticsReportsLink').click(function(e){
        e.preventDefault();
        $(this).parent().addClass('active');
        $('#myFinancialStatementLink').parent().removeClass('active');
        $('#navBar').hide(200);

        $.post( "statistics_Reports/index_statistics_Reports.cfm", function( data ) { $( "#mainContentContainer" ).html( data ); });
    });
</script>

<script type="text/javascript"><!--- Loading animation --->

  $('.loading').each(function(index) {
    a = new Sonic(loaders[6]);
    a.play();
      $(this).html(a.canvas);
  });

</script>