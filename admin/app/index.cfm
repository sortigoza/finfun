<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - This is the main file that loads when the user is loged to the Fin-Fun web application.
// - This file contains the includes of css and javascripts files, the main navigation and layout of the three main sections.
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 31/05/2014
////////////////////////////////////////////////////// --->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="keywords" content="Personal finances, finances, financial tool">
    <meta name="description" content="personal finances tool">
    <meta name="author" content="Saul Ortigoza">
    <link rel="icon" href="/assets/ico/Dollar.ico" type="image/x-icon">
    <!---  Experimental --->
    

    <title>Fin-Fun</title>

    <!--- CSS Fonts --->
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'><!--- Load Font --->
    <link href='http://fonts.googleapis.com/css?family=Nova+Square|Allerta|Roboto' rel='stylesheet' type='text/css'><!--- Load Font --->
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Nunito' rel='stylesheet' type='text/css'>

    <!--- CSS --->
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css">

    <link href="/css/main-default.css" rel="stylesheet" type="text/css">
    <link href="/css/financialSt-theme-default.css" rel="stylesheet" type="text/css">
    <link href="/css/financialSt-theme-one.css" rel="stylesheet" type="text/css">

    <!--- JavaScript --->
    <script type="text/javascript" src="/javascript/jquery.js"></script><!--- JQuery --->
    <script type="text/javascript" src="/javascript/bootstrap.js"></script> <!--- Bootstrap --->
    <cfinclude template="/javascript/loading-animation.cfm"><!--- Loading animation --->
    <!---
    <script type="text/javascript" src="https://www.google.com/jsapi"></script><!--- Google Charts --->
    --->
  </head>

  <!--- Body Starts --->
  <body>
    <cfif session.user.userClass EQ 1>
      <cfinclude template="index_class_1.cfm">
    <cfelseif session.user.userClass EQ 0>
      <cfinclude template="index_class_0.cfm">
    </cfif>
  </body>
</html>

<script type="text/javascript">
      $('#closeForm').click(function(e){
      e.preventDefault();
      $('#blinde').hide();
      });

      $('#logoutButton').click(function(e){
       window.location.href = "/logout.cfm";
      });
</script>

