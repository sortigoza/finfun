<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - <text>
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 
////////////////////////////////////////////////////// --->
<cfset finman = CreateObject("cfc\financialStManager") />
<cfset finfunc = CreateObject("cfc\financialStFunctions") />


<cfset expenses = finfunc.Get_Expenses_All_Depth(#session.user.userRootFSID#)>
<cfset expense_set = CreateObject("cfc\Expense_Set_obj").init(#expenses#) />
<cfset incomes = finfunc.Get_Incomes_All_Depth(#session.user.userRootFSID#)>
<cfset income_set = CreateObject("cfc\Income_Set_obj").init(#incomes#) />


<!--- Main FS Container --->
<cfinclude template="statistics_Incomes/index_statistics_incomes_plots.cfm">
<cfinclude template="statistics_Incomes/index_statistics_incomes.cfm">
<cfinclude template="statistics_Incomes/index_statistics_expenses.cfm">