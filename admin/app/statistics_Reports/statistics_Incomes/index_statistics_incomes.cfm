<!---
<cfset incomes = finfunc.Get_Incomes_All_Depth(#session.user.userRootFSID#)>
<cfset income_set = CreateObject("cfc\Income_Set_obj").init(#incomes#) />
--->
<div class="panel panel-default" style="margin-top: 20px;">
  <div class="panel-body">
    <h4 class="panel-title">
          Incomes
      </h4>
  </div>
</div>

<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Top 10
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
      <div class="panel-body">
        <!--- Top 10 Body --->
        <table class="table table-hover" id="incomes_table">
          <thead>
              <tr>
                <th width="5%"><cfoutput>##</cfoutput></th>
                <th width="10%">ID</th>
                <th width="40%">Name</th>
                <th width="30%">Date</th>
                <th width="30%">Amount</th>
              </tr>
          </thead>
          <tbody>
            <cfset temp = income_set.top()>
            <cfoutput query="temp">
                <tr incomeID="#DBINCOME_ID#">
                    <td>#CurrentRow#</td>
                    <td>#DBINCOME_ID#</td>
                    <td>#DBINCOME_NAME#</td>
                    <td>#DateFormat(DBINCOME_CREATIONDATE, "mmm-dd-yyyy")#</td>
                    <td>#DBINCOME_AMOUNT#</td>
                </tr>
                <cfset lastCurrentRow = #CurrentRow#>
            </cfoutput>
          </tbody>
        </table>
        <!--- Top 10 Body. End! --->
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          Order Statistics
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <!--- Order Statistics --->
          <cfoutput>
          <table class="table" id="incomes_table">
            <thead>
                <tr>
                  <th>Number of Incomes</th>
                  <th>Total Income</th>
                  <th>Maximum Income</th>
                  <th>Minimum Income</th>
                  <th>Range</th>
                </tr>
            </thead>
            <tbody>
                  <tr>
                      <td>#income_set.count()#</td>
                      <td>#numberFormat(income_set.sum(),'-____.__')#</td>
                      <td>#numberFormat(income_set.max(),'-____.__')#</td>
                      <td>#numberFormat(income_set.min(),'-____.__')#</td>
                      <td>#numberFormat(income_set.range(),'-____.__')#</td>
                  </tr>
            </tbody>
          </table>
          </cfoutput>
        <!--- Order Statistics. End! --->
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          Averages And Variances
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        <!--- Averages And Variances --->
          <cfoutput>
          <table class="table" id="incomes_table">
            <thead>
                <tr>
                  <th>Mean</th>
                  <th>Median</th>
                  <th>Standard Deviation</th>
                  <th>Variance</th>
                </tr>
            </thead>
            <tbody>
                  <tr>
                      <td>#numberFormat(income_set.mean(),'-____.__')#</td>
                      <td>#numberFormat(income_set.median(),'-____.__')#</td>
                      <td>#numberFormat(income_set.std(),'-____.__')#</td>
                      <td>#numberFormat(income_set.var(),'-____.__')#</td>
                  </tr>
            </tbody>
          </table>
          </cfoutput>
        <!--- Averages And Variances. End! --->
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" id="histogram_click">
          Histogram
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
      <div class="panel-body" id="histogram_container">

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $('#histogram_click').click(function(e){
        e.preventDefault();
        $.post( 'statistics_Reports/statistics_Incomes/incomes_histogram.cfm', {arrray: "<cfset hist_data = income_set.data_as_string() />"}, function(data){ 
                $( "#histogram_container" ).html( data ); 
            }
        );
    });
</script>