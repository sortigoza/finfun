<!---
<cfset expenses = finfunc.Get_Expenses_All_Depth(#session.user.userRootFSID#)>
<cfset expense_set = CreateObject("cfc\Expense_Set_obj").init(#expenses#) />
--->
<div class="panel panel-default">
  <div class="panel-body">
    <h4 class="panel-title">
          Expenses
      </h4>
  </div>
</div>

<div class="panel-group" id="accordion_expenses">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion_expenses" href="#collapseOne_expenses">
          Top 10
        </a>
      </h4>
    </div>
    <div id="collapseOne_expenses" class="panel-collapse collapse">
      <div class="panel-body">
        <!--- Top 10 Body --->
        <table class="table table-hover" id="expenses_table">
          <thead>
            <tr>
              <th width="5%"><cfoutput>##</cfoutput></th>
              <th width="10%">ID</th>
              <th width="40%">Name</th>
              <th width="30%">Date</th>
              <th width="30%">Amount</th>
            </tr>
          </thead>
          <tbody>
            <cfset temp = expense_set.top()>
            <cfoutput query="temp">
                <tr expenseID="#DBEXPENSE_ID#">
                    <td>#CurrentRow#</td>
                    <td>#DBEXPENSE_ID#</td>
                    <td>#DBEXPENSE_NAME#</td>
                    <td>#DateFormat(DBEXPENSE_CREATIONDATE, "mmm-dd-yyyy")#</td>
                    <td>#DBEXPENSE_AMOUNT#</td>
                </tr>
                <cfset lastCurrentRow = #CurrentRow#>
            </cfoutput>
          </tbody>
        </table>
        <!--- Top 10 Body. End! --->
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion_expenses" href="#collapseTwo_expenses">
          Order Statistics
        </a>
      </h4>
    </div>
    <div id="collapseTwo_expenses" class="panel-collapse collapse">
      <div class="panel-body">
        <!--- Order Statistics --->
          <cfoutput>
          <table class="table" id="incomes_table">
            <thead>
                <tr>
                  <th>Number of Expenses</th>
                  <th>Total Expenses</th>
                  <th>Maximum Expense</th>
                  <th>Minimum Expense</th>
                  <th>Range</th>
                </tr>
            </thead>
            <tbody>
                  <tr>
                      <td>#expense_set.count()#</td>
                      <td>#numberFormat(expense_set.sum(),'-____.__')#</td>
                      <td>#numberFormat(expense_set.max(),'-____.__')#</td>
                      <td>#numberFormat(expense_set.min(),'-____.__')#</td>
                      <td>#numberFormat(expense_set.range(),'-____.__')#</td>
                  </tr>
            </tbody>
          </table>
          </cfoutput>
        <!--- Order Statistics. End! --->
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion_expenses" href="#collapseThree_expenses">
          Averages And Variances
        </a>
      </h4>
    </div>
    <div id="collapseThree_expenses" class="panel-collapse collapse">
      <div class="panel-body">
        <!--- Averages And Variances --->
          <cfoutput>
          <table class="table" id="incomes_table">
            <thead>
                <tr>
                  <th>Mean</th>
                  <th>Median</th>
                  <th>Standard Deviation</th>
                  <th>Variance</th>
                </tr>
            </thead>
            <tbody>
                  <tr>
                      <td>#numberFormat(expense_set.mean(),'-____.__')#</td>
                      <td>#numberFormat(expense_set.median(),'-____.__')#</td>
                      <td>#numberFormat(expense_set.std(),'-____.__')#</td>
                      <td>#numberFormat(expense_set.var(),'-____.__')#</td>
                  </tr>
            </tbody>
          </table>
          </cfoutput>
        <!--- Averages And Variances. End! --->
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion_expenses" href="#collapseFour_expenses">
          Histogram
        </a>
      </h4>
    </div>
    <div id="collapseFour_expenses" class="panel-collapse collapse">
      <div class="panel-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
</div>