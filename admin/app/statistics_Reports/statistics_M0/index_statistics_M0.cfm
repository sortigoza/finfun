<cfset finman = CreateObject("cfc\financialStManager") />
<cfset finfunc = CreateObject("cfc\financialStFunctions") />

<div class="panel panel-default">
  <div class="panel-body">
    Order Statistics
  </div>
</div>

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">M0 - Statistics Report</h3>
  </div>
  <div class="panel-body">
    <cfset incomes = finfunc.Get_Incomes_All_Depth(#session.user.userRootFSID#)>
    <table class="table table-hover" id="incomes_table">
        <thead>
            <tr>
                <th width="5%"><cfoutput>##</cfoutput></th>
                <th width="10%">ID</th>
                <th width="40%">Name</th>
                <th width="30%">Date</th>
                <th width="30%">Amount</th>
            </tr>
        </thead>
        <tbody>
            <cfoutput query="incomes">
                <tr incomeID="#DBINCOME_ID#">
                    <td>#DBINCOME_ID#</td>
                    <td>#CurrentRow#</td>
                    <td>#DBINCOME_NAME#</td>
                    <td>#DateFormat(DBINCOME_CREATIONDATE, "mmm-dd-yyyy")#</td>
                    <td>#DBINCOME_AMOUNT#</td>
                </tr>
                <cfset lastCurrentRow = #CurrentRow#>
            </cfoutput>
        </tbody>
    </table>


        <cfoutput>
            Total incomes: #incomes.RecordCount# <br>
            <cfset mean = Get_Mean(#incomes#)>
            Mean: #mean#
        </cfoutput>
  </div>
</div>

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">M0 - Statistics Report</h3>
  </div>
  <div class="panel-body">
    <cfset expenses = finfunc.Get_Expenses_All_Depth(#session.user.userRootFSID#)>
    <table class="table table-hover" id="incomes_table">
    <thead>
        <tr>
            <th width="5%"><cfoutput>##</cfoutput></th>
            <th width="10%">ID</th>
            <th width="40%">Name</th>
            <th width="30%">Date</th>
            <th width="30%">Amount</th>
        </tr>
    </thead>
    <tbody>
        <cfoutput query="expenses">
            <tr incomeID="#DBEXPENSE_ID#">
                <td>#DBEXPENSE_ID#</td>
                <td>#CurrentRow#</td>
                <td>#DBEXPENSE_NAME#</td>
                <td>#DateFormat(DBEXPENSE_CREATIONDATE, "mmm-dd-yyyy")#</td>
                <td>#DBEXPENSE_AMOUNT#</td>
            </tr>
            <cfset lastCurrentRow = #CurrentRow#>
        </cfoutput>
    </tbody>
    </table>

   
        <cfoutput>
            Total expenses: #expenses.RecordCount# <br>
        </cfoutput>
  </div>
</div>






<cffunction name="Get_Mean" output="true" access= "public" returnType="string">
        <cfargument name="dataQuery" type="query" required="true" default="" />
        <cfquery name="result" dbtype="query">
            SELECT AVG(DBINCOME_AMOUNT) AS result FROM arguments.dataQuery;
        </cfquery>
        <cfreturn #result.result#>  
</cffunction>

<cffunction name="Get_Mode" output="true" access= "public" returnType="string">
        <cfargument name="dataQuery" type="query" required="true" default="" />
        <cfquery name="result" dbtype="query">
            SELECT AVG(DBINCOME_AMOUNT) AS result FROM arguments.dataQuery;
        </cfquery>
        <cfreturn #result.result#>  
</cffunction>

<cffunction name="Get_Median" output="true" access= "public" returnType="string">
        <cfargument name="dataQuery" type="query" required="true" default="" />
        <cfquery name="result" dbtype="query">
            SELECT AVG(DBINCOME_AMOUNT) AS result FROM arguments.dataQuery;
        </cfquery>
        <cfreturn #result.result#>  
</cffunction>