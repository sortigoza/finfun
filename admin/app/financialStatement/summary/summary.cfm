<cfset finfunc = CreateObject("cfc\financialStFunctions") />
<cfset finman = CreateObject("cfc\financialStManager") />
<!--- Total Income +  Assets Income --->
<cfset getassets = finman.get_assets(#session.user.userCurrentFSID#)>
<cfset getincomes = finman.get_incomes(#session.user.userCurrentFSID#)>
<cfset TotalIncomeAllDepth = 0>
<cfloop query="getassets">
<cfset TotalIncomeAllDepth = TotalIncomeAllDepth + finfunc.Get_NetIncome_All_Depth(#DBASSET_CHILDFS_ID#)>
</cfloop>
<cfloop query="getincomes">
<cfset TotalIncomeAllDepth = TotalIncomeAllDepth + #DBINCOME_AMOUNT#>
</cfloop>
<!---Total Expense + Liability Expense --->
<cfset getliabilities = finman.get_liabilities(#session.user.userCurrentFSID#)>
<cfset getexpenses = finman.get_expenses(#session.user.userCurrentFSID#)>
<cfset TotalExpenseAllDepth = 0>
<cfloop query="getliabilities">
<cfset TotalExpenseAllDepth = TotalExpenseAllDepth - finfunc.Get_NetIncome_All_Depth(#DBLIABILITY_CHILDFS_ID#)>
</cfloop>
<cfloop query="getexpenses">
<cfset TotalExpenseAllDepth = TotalExpenseAllDepth + #DBEXPENSE_AMOUNT#>
</cfloop>
<!--- Net Income --->
<cfset NetIncomeAllDepth = #TotalIncomeAllDepth# - #TotalExpenseAllDepth#>

<!--- Assets --->
<cfset asset_E_D = finfunc.Get_Assets_E_D_All_Depth(#session.user.userCurrentFSID#)>
<cfset AssetEquity = #asset_E_D.ASSETEQUITY# >
<cfset AssetDebt = #asset_E_D.ASSETDEBT# >
<!--- Liabilities --->
<cfset Liability_E_D = finfunc.Get_Liabilities_E_D_All_Depth(#session.user.userCurrentFSID#)>
<cfset LiabilityEquity = #Liability_E_D.LiabilityEQUITY#>
<cfset LiabilityDebt = #Liability_E_D.LiabilityDEBT#>
<!---- Net value---->
<cfset NetValueAllDepth = (AssetEquity + LiabilityEquity) - (AssetDebt + LiabilityDebt)>
<!---- Unit Vector + Magnitude ---->
<cfset FSmagnitude = TotalIncomeAllDepth*TotalIncomeAllDepth + TotalExpenseAllDepth*TotalExpenseAllDepth>
<cfset FSmagnitude = FSmagnitude + (AssetEquity - AssetDebt)*(AssetEquity - AssetDebt)>
<cfset FSmagnitude = FSmagnitude + (LiabilityEquity - LiabilityDebt)*(LiabilityEquity - LiabilityDebt)>
<cfset FSmagnitude = Sqr(FSmagnitude)>
<cfif FSmagnitude NEQ 0>
    <cfset FSunitVector = [TotalIncomeAllDepth/FSmagnitude, TotalExpenseAllDepth/FSmagnitude, (AssetEquity - AssetDebt)/FSmagnitude, (LiabilityEquity - LiabilityDebt)/FSmagnitude]>
<cfelse>
    <cfset FSunitVector = [0,0,0,0]>
</cfif>
<!--- Pre-formating --->
<cfset AssetEquity = numberFormat(#asset_E_D.ASSETEQUITY#,'-_,___.__') >
<cfset AssetDebt = numberFormat(#asset_E_D.ASSETDEBT#,'-_,___.__') >
<cfset LiabilityEquity = numberFormat(#Liability_E_D.LiabilityEQUITY#,'-_,___.__')>
<cfset LiabilityDebt = numberFormat(#Liability_E_D.LiabilityDEBT#,'-_,___.__')>
<cfset NetValueAllDepth = numberFormat(#NetValueAllDepth#,'-_,___.__')>
<cfset TotalIncomeAllDepth = numberFormat(#TotalIncomeAllDepth#,'-_,___.__')>
<cfset TotalExpenseAlldepth = numberFormat(#TotalExpenseAlldepth#,'-_,___.__')>
<cfset NetIncomeAllDepth = numberFormat(#NetIncomeAllDepth#,'-_,___.__')>


<!--- Output --->
<cfoutput>
<table class="table" id="summary_table">
    <thead>
        <tr>
            <th width="50%">Subject</th>
            <th width="50%">Result</th>
        </tr>
    </thead>
    <tbody>
            <tr>
                <td>Total Incomes:</td>
                <td>#TotalIncomeAllDepth#</td>
            </tr>
            <tr>
                <td>Total Expenses:</td>
                <td >#TotalExpenseAlldepth# </td>
            </tr>
            <tr class="active">
                <td>Net Income:</td>
                <td >#NetIncomeAllDepth# </td>
            </tr>
            <tr>
                <td></td>
                <td ></td>
            </tr>
            <tr>
                <td>Total Assets Equity:</td>
                <td >#AssetEquity#</td>
            </tr>
            <tr>
                <td>Total Assets Debt:</td>
                <td >#AssetDebt#</td>
            </tr>
            <tr>
                <td>Total Liabilities Equity:</td>
                <td >#LiabilityEquity#</td>
            </tr>
            <tr>
                <td>Total Liabilities Debt:</td>
                <td >#LiabilityDebt#</td>
            </tr>
            <tr class="active">
                <td>Net value:</td>
                <td >#NetValueAllDepth#</td>
            </tr>
    </tbody>
</table>

<table class="table" id="vector_table">
    <tr>
        <td> #numberFormat(FSmagnitude ,'-_,___.__')#</td>
        <cfloop array=#FSunitVector# index="unit">
            <td>#numberFormat(unit ,'-_.____')#</td>
        </cfloop>
    </tr>
</table>
</cfoutput>

