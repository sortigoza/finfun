<script type="text/javascript">
// financialStatementLoad(Period, From, To)
function FinancialStatementLoad(){
  $.post( "financialStatement/incomeStatement/incomeStatement_incomes.cfm", function( data ) {
    $( "#incomesContent" ).html( data );
  });
  $.post( "financialStatement/incomeStatement/incomeStatement_expenses.cfm", function( data ) {
    $( "#expensesContent" ).html( data );
  });
  $.post( "financialStatement/balanceSheet/balanceSheet_assets.cfm", function( data ) {
    $( "#assetsContent" ).html( data );
  });
  $.post( "financialStatement/balanceSheet/balanceSheet_Liabilities.cfm", function( data ) {
    $( "#liabilitiesContent" ).html( data );
  });
  $.post( "financialStatement/Summary/Summary.cfm", function( data ) {
    $( "#summaryContent" ).html( data );
  });
  $( "#EventHandlerDebug" ).append( " FinancialStatementLoad." );
}

//Click root button event
$('#rootButton').click(function(e){
  e.preventDefault();
  $('#EventHandlerDebug').html("Click root button event");

  $.post( "financialStatement/DL_Mtd_TreeNavigation_API.cfm", {FSNavFunction: "Load_Root"}, function( data ) {
    $( "#EventHandlerDebug" ).append( data );
  });
  FinancialStatementLoad();
});

//Click up button event
$('#upButton').click(function(e){
  e.preventDefault();
  $('#EventHandlerDebug').html("Click up button event");

  $.post( "financialStatement/DL_Mtd_TreeNavigation_API.cfm", {FSNavFunction: "Load_UP"}, function( data ) {
    $( "#EventHandlerDebug" ).append( data );
  });
  FinancialStatementLoad();
});

//Modal form change event 
$('#modeForm').change(function() {
  $('#EventHandlerDebug').html("Modal form change event ");
  var modeval = this.PeriodUnitID.value;
  //console.log( modeFormData );

  switch(modeval) {
    case '3':
        $('#monthSelect').attr('disabled',true);
        $('#yearSelect').attr('disabled',true);
        break;
    case '2':
        $('#monthSelect').attr('disabled',true);
        $('#yearSelect').attr('disabled',false);
        break;
    case '1':
        $('#monthSelect').attr('disabled',false);
        $('#yearSelect').attr('disabled',false);
        break;
  }

  $('#FSNavFunction').val("Refresh_Filter");
  var modeFormData = $(this).serialize();
  $.post( "financialStatement/DL_Mtd_TreeNavigation_API.cfm", modeFormData, function( data ) {
    $( "#EventHandlerDebug" ).append( data );
  });
});
</script>

<div id="EventHandlerDebug" style="background-color: #FFFF99; padding: 15px; display: none">
  FS Event Handler Loaded!
  <cfoutput> #session.user.userRootFSID#</cfoutput>
  <cfoutput> #session.user.userCurrentFSID#</cfoutput>
</div>


