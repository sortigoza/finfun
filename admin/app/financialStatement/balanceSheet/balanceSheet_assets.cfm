<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - <text>
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 
////////////////////////////////////////////////////// --->

<cfset finman = CreateObject("cfc\financialStManager") />
<cfset getassets = finman.get_assets(#session.user.userCurrentFSID#)>

<table class="table table-hover" id="assets_table">
    <thead>
        <tr>
            <th width="5%"><cfoutput>##</cfoutput></th>
            <th width="45%">Name</th>
            <th width="25%">Equity</th>
            <th width="25%">Debt</th>
        </tr>
    </thead>
    <tbody>
        <cfoutput query="getassets">
            <tr assetID="#DBASSET_ID#" assetChildID="#dbAsset_ChildFS_ID#">
                <td>#CurrentRow#</td>
                <td>#DBASSET_NAME#</td>
                <td class="assetCost">#dbAsset_Equity#</td>
                <td class="assetCost">#dbAsset_Debt#</td>
            </tr>
        </cfoutput>
    </tbody>
</table>


<script type="text/javascript">
    $('#assets_table tr').click(function(e){
        e.preventDefault();
        $('#EventHandlerDebug').html("FS Load event. ");
        var assetChildID = $(this).attr('assetChildID');  
        $('#FSNavFunction').val("Load_FS");
        $('#FSID').val(assetChildID);
        var modeFormData = $('#modeForm').serialize();
        $.post( "financialStatement/DL_Mtd_TreeNavigation_API.cfm", modeFormData, function( data ) { $( "#EventHandlerDebug" ).append(  data ); });
        FinancialStatementLoad();
    });
</script>
