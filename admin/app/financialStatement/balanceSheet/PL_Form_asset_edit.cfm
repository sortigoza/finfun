<cfif #session.user.userTempVar1# EQ "asset">
<cfset finman = CreateObject("cfc\financialStManager") />
<cfset data = finman.Retrieve_asset_data(#session.user.userTempVar2#)>

<div class="modal fade" id="AssetEdit_Modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <cfoutput query="data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <div class="modal-title">Asset Edit: #DBASSET_NAME#</div>
        </div>
        <div class="modal-body">     

          <form class="form-horizontal Form" role="form" method="post" action="" id="Form">
            <input type="hidden" name="FSMFunction" value="BalanceSheet_Update_Asset"/>
            <!--- Asset ID --->
            <input type="hidden" id="Asset_ID" name="Asset_ID" 
              value="#DBASSET_ID#"
            />
            <!--- FS ID --->
            <input type="hidden" name="FinancialSt_ID" 
              value="#DBASSET_ID#"
            />
            <!--- Name --->
            <div class="form-group">
              <label for="Name" class="col-sm-2 control-label">Asset Name: </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="Name" placeholder="Asset Name" required="true" name="Name" maxlength="60"
                  value="#DBASSET_NAME#"
                />
              </div>
            </div>
            <!--- Description --->
            <div class="form-group">
              <label for="Description" class="col-sm-2 control-label">Description: </label>
              <div class="col-sm-10">
                <textarea type="text" class="form-control" id="Description" placeholder="Description" rows="3" required="true" name="Description">#DBASSET_DESCRIPTION#</textarea>
              </div>
            </div>
            <!--- E/D --->
            <div class="form-group">
              <label for="Equity" class="col-sm-2 control-label">Equity: </label>
              <div class="col-sm-10">
                <input type="number" step="any" class="form-control" id="Equity" placeholder="Equity" required="true" name="Equity"
                  value="#DBASSET_EQUITY#"
                />
              </div>
            </div>
            <div class="form-group">
              <label for="Debt" class="col-sm-2 control-label">Debt: </label>
              <div class="col-sm-10">
                <input type="number" step="any" class="form-control" id="Debt" placeholder="Debt" required="true" name="Debt"
                  value="#DBASSET_DEBT#"
                />
              </div>
            </div>
            <!--- Date --->
            <div class="form-group">
              <label for="Date" class="col-sm-2 control-label">Date: </label>
              <div class="col-sm-10">
                <input type="date" class="form-control" id="Date" placeholder="Date" required="true" name="Date"
                  value="#DateFormat(DBASSET_CREATIONDATE, "YYYY-MM-DD")#"
                />
              </div>
            </div>
            <!--- Category --->
            <!--- Submit button 
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default" id="save">Submit </button>
              </div>
            </div> --->
          </form>
        </cfoutput>

        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default" id="saveChangesButton"><span class="glyphicon glyphicon-pencil"></span> Save Changes</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="cancelButton"><span class="glyphicon glyphicon-minus"></span> Cancel</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$( document ).ready(function() {
  $('.modal').modal('show');
});
</script>

<script type="text/javascript" src="\admin\app\financialStatement\DL_Mtd_FinancialStatementAJAX.js"></script>
<script type="text/javascript">

$( '#saveChangesButton' ).click(function(e) {
  e.preventDefault();
  var FormData = $('#Form').serialize();
  //console.log( 'form data: ' + $( "#Form" ).serialize() );

  $.post("/cfc/financialStManager_API.cfm", FormData , function( data ) {
    $('.modal-body').empty();
    $('.modal-body').append(data);
  })
    .fail(function() {
    $('.modal-body').empty();
    $('.modal-body').append('<span class="glyphicon glyphicon-remove-sign"></span> Error! Try again later...!<br/>');
  });

  $('#saveChangesButton').hide();
  $('#cancelButton').html('<span class="glyphicon glyphicon-minus"></span> Close');
  
   FinancialStatementLoad();
});
</script>

</cfif>