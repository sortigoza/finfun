<cfset finman = CreateObject("cfc\financialStManager") />
Form: <br>
<cfdump var=#form#>

<!---  in case of call of posting an asset, create asset. --->
<cfif IsDefined("form.assetName")>
	<cfset assetID = finman.Create_asset(#form.assetName#, #form.FINANCIALST_ID#, #form.assetDescription#, #form.assetAMOUNT#, #form.assetDATE#)>
	Your asset, with ID: <cfoutput>#assetID#</cfoutput>, was successfuly added to te Database.
</cfif>

<!---  in case of call of posting an liability, create liability. --->
<cfif IsDefined("form.liabilityNAME")>
	<cfset liabilityID = finman.Create_liability(#form.liabilityNAME#, #form.FINANCIALST_ID#, #form.liabilityDESCRIPTION#, #form.liabilityAMOUNT#, #form.liabilityDATE#)>
	Your liability, with ID: <cfoutput>#liabilityID#</cfoutput>, was successfuly added to te Database.
</cfif>


<script type="text/javascript">
$( document ).ready(function() {
  FinancialStatementReload();
});
</script>
