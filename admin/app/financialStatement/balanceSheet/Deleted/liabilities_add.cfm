<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading formHeading">Liability Add</div>
  <div class="panel-body validationDivmsg">
    <p>...Please fill all data of your new liability.</p>

  <form class="form-horizontal liabilityForm" role="form" method="post" action="" id="liabilityForm">
    <!--- Current FS ID --->
    <input type="hidden" name="FinancialSt_ID" value="<cfoutput>#session.user.userCurrentFSID#</cfoutput>"/>
    <!--- Name --->
    <div class="form-group">
      <label for="liabilityName" class="col-sm-2 control-label">Liability Name: </label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="liabilityName" placeholder="Liability Name" required="true" name="liabilityName">
      </div>
    </div>
    <!--- Description --->
    <div class="form-group">
      <label for="Description" class="col-sm-2 control-label">Description: </label>
      <div class="col-sm-10">
        <textarea type="text" class="form-control" id="Description" placeholder="Description" rows="3" required="true" name="liabilityDescription"></textarea>
      </div>
    </div>
    <!--- Amount --->
    <div class="form-group">
      <label for="Amount" class="col-sm-2 control-label">Amount: </label>
      <div class="col-sm-10">
        <input type="number" class="form-control" id="Amount" placeholder="Amount" required="true" name="liabilityAmount">
      </div>
    </div>
    <!--- Date --->
    <div class="form-group">
      <label for="Date" class="col-sm-2 control-label">Date: </label>
      <div class="col-sm-10">
        <input type="date" class="form-control" id="Date" placeholder="Date" required="true" name="liabilityDate">
      </div>
    </div>
    <!--- Category --->
    <!--- Submit button --->
    <div class="form-group">
      <div class="col-md-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default" id="saveLiability">Submit </button>
      </div>
    </div>
    
  </form>
  </div>
</div>



<script type="text/javascript">
$( '#liabilityForm' ).submit(function(e) {
  e.preventDefault();
  var liabilityFormData = $(this).serialize();
  console.log( 'form data: ' + $( "form" ).serialize() );

  $.ajax({
      type: 'POST',
      url: 'financialStatement/balanceSheet/AL_Mtd_balanceSheet_post.cfm',
      data: liabilityFormData,
      error: function(){
        $('.validationDivmsg').empty();
        $('.validationDivmsg').append('<span class="glyphicon glyphicon-remove-sign"></span> Error! Try again later...!<br/>');
      },
      success: function(data){
        $('.validationDivmsg').empty();
        $('.validationDivmsg').append(data);
        }
  });
});
</script>