<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - <text>
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 
////////////////////////////////////////////////////// --->
<cfset finman = CreateObject("cfc\financialStManager") />
<cfset getliabilities = finman.get_liabilities(#session.user.userCurrentFSID#)>

<table class="table table-hover" id="liabilities_table">
    <thead>
        <tr>
            <th width="5%"><cfoutput>##</cfoutput></th>
            <th width="45%">Name</th>
            <th width="25%">Equity</th>
            <th width="25%">Debt</th>
        </tr>
    </thead>
    <tbody>
        <cfoutput query="getliabilities">
            <tr liabilityID="#DBLIABILITY_ID#" liabilityChildID="#dbLiability_ChildFS_ID#">
                <td>#CurrentRow#</td>
                <td>#DBLIABILITY_NAME#</td>
                <td class="liabilityCost">#dbLiability_Equity#</td>
                <td class="liabilityCost">#dbLiability_Debt#</td>
            </tr>
        </cfoutput>
    </tbody>
</table>

<script type="text/javascript">
    $('#liabilities_table tr').click(function(e){
        e.preventDefault();
        $('#EventHandlerDebug').html("FS Load event. ");
        var liabilityChildID = $(this).attr('liabilityChildID');  
        $('#FSNavFunction').val("Load_FS");
        $('#FSID').val(liabilityChildID);
        var modeFormData = $('#modeForm').serialize();

        $.post( "financialStatement/DL_Mtd_TreeNavigation_API.cfm", modeFormData, function( data ) {$( "#EventHandlerDebug" ).append(  data ); });
    });
</script>

