<div class="modal fade" id="AssetAdd_Modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div class="modal-title">Asset Add</div>
      </div><!-- /.modal-header -->
      <div class="modal-body">  

        <form class="form-horizontal assetForm" role="form" method="post" action="" id="assetForm">
          <input type="hidden" name="FSMFunction" value="BalanceSheet_Add_Asset"/>
          <!--- Current FS ID --->
          <input type="hidden" name="FinancialSt_ID" value="<cfoutput>#session.user.userCurrentFSID#</cfoutput>"/>
          <!--- Name --->
          <div class="form-group">
            <label for="assetName" class="col-sm-2 control-label">Asset Name: </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="assetName" placeholder="Asset Name" required="true" name="assetName">
            </div>
          </div>
          <!--- Description --->
          <div class="form-group">
            <label for="Description" class="col-sm-2 control-label">Description: </label>
            <div class="col-sm-10">
              <textarea type="text" class="form-control" id="Description" placeholder="Description" rows="3" required="true" name="assetDescription"></textarea>
            </div>
          </div>
          <!--- E/D --->
          <div class="form-group">
            <label for="Equity" class="col-sm-2 control-label">Equity: </label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="Equity" placeholder="Equity" required="true" name="assetEquity">
            </div>
          </div>
          <div class="form-group">
            <label for="Debt" class="col-sm-2 control-label">Debt: </label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="Debt" placeholder="Debt" required="true" name="assetDebt">
            </div>
          </div>
          <!--- Date --->
          <div class="form-group">
            <label for="Date" class="col-sm-2 control-label">Date: </label>
            <div class="col-sm-10">
              <input type="date" class="form-control" id="Date" placeholder="Date" required="true" name="assetDate" value="<cfoutput>#DateFormat(Now(), "YYYY-MM-DD")#</cfoutput>">
            </div>
          </div>
          <!--- Category --->
        </form><!-- /.form -->
        
      </div><!-- /.modal-body -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-default" id="addButton"><span class="glyphicon glyphicon-plus"></span> Add Asset</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="cancelButton"><span class="glyphicon glyphicon-minus"></span> Cancel</button>
      </div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script type="text/javascript">
$( document ).ready(function() {
  $('.modal').modal('show');
});

$( '#addButton' ).click(function(e) {
  e.preventDefault();
  var assetFormData = $('#assetForm').serialize();
  //console.log( 'form data: ' + $( "form" ).serialize() );
  $.post("/cfc/financialStManager_API.cfm", assetFormData , function( data ) {
    $('.modal-body').empty();
    $('.modal-body').append(data);
  })
    .fail(function() {
    $('.modal-body').empty();
    $('.modal-body').append('<span class="glyphicon glyphicon-remove-sign"></span> Error! Try again later...!<br/>');
  });
    
  $('#addButton').hide();
  $('#cancelButton').html('<span class="glyphicon glyphicon-minus"></span> Close');
  FinancialStatementLoad();
});
</script>