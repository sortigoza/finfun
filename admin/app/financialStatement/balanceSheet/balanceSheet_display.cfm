<cfset finman = CreateObject("cfc\financialStManager") />
<cfset data = finman.Get_FS_Data_full(#session.user.userCurrentFSID#)>

<div class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!--- Asset --->
      <cfif isDefined("data.DBASSET_ID")>
        <cfset session.user.userTempVar1 = "asset"><!--- Internal interchange of data --->
        <cfset session.user.userTempVar2 = #data.DBAsset_ID#><!--- Internal interchange of data --->
        <cfoutput query="data">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Asset Display: #DBASSET_NAME#</h4>
          </div>
          <div class="modal-body display-modal-body">
            <table class="table table-bordered display-table">
              <tr bgcolor="##e0e0e0">
                <td>Name</td>
                <td>Asset ID</td>
                <td colspan="2">Creation Date</td>
              </tr>
              <tr>
                <td>#DBASSET_NAME#</td>
                <td>#DBASSET_ID#</td>
                <td>#DateFormat(DBASSET_CREATIONDATE, "ddd, mmmm dd, yyyy")#</td>
              </tr>
              <tr bgcolor="##e0e0e0">
                <td>Equity</td>
                <td>Debt</td>
                <td>Net Value</td>
              </tr>
              <tr>
                <td>#DBASSET_EQUITY#</td>
                <td>#DBASSET_DEBT#</td>
                <td>#numberFormat(DBASSET_EQUITY - DBASSET_DEBT,'-____.__')#</td>
              </tr>
              <tr>
                <td colspan="4" bgcolor="##e0e0e0">Description:</td>
              </tr>
              <tr>
                <td colspan="4">#DBASSET_DESCRIPTION#</td>
              </tr>
            </table> 	
          </div>

        </cfoutput>
      </cfif>

      <!--- Liability --->
      <cfif isDefined("data.DBLIABILITY_ID")>
        <cfset session.user.userTempVar1 = "liability"><!--- Internal interchange of data --->
        <cfset session.user.userTempVar2 = #data.DBLiability_ID#><!--- Internal interchange of data --->
        <cfoutput query="data">

           <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">LIABILITY Display: #DBLIABILITY_NAME#</h4>
          </div>
          <div class="modal-body display-modal-body">
  	          	Name: #DBLIABILITY_NAME# <br>
  	          	Liability ID: #DBLIABILITY_ID# <br> 
  	          	Cost: #DBLIABILITY_EQUITY# <br>
                Cost: #DBLIABILITY_DEBT# <br>
  	          	Date: #DateFormat(DBLIABILITY_CREATIONDATE, "ddd, mmmm dd, yyyy")# <br>
  	          	Description: <br>
  	          	#DBLIABILITY_DESCRIPTION# <br><br>    
          </div>

        </cfoutput>
      </cfif>


      <div class="modal-footer display-modal-footer">
        <button type="button" class="btn btn-default" id="DeleteButton" <cfif session.user.userRootFSID EQ session.user.userCurrentFSID>disabled="disabled"</cfif>><span class="glyphicon glyphicon-trash"></span> Delete</button>
        <button type="button" class="btn btn-default" id="MoveButton" disabled="disabled"><span class="glyphicon glyphicon-share-alt"></span> Move</button>
        <button type="button" class="btn btn-default" id="EditButton"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-minus"></span> Close</button>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$( document ).ready(function() {
  $('.modal').modal('show');
});

$('#DeleteButton').click(function(e){
  e.preventDefault();
  $.post("/cfc/financialStManager_API.cfm", { FSMFunction: "BalanceSheet_Delete_Asset_Liability"}, function( data ) {
    $( ".modal-body" ).html( data );
  });
  $( ".modal-body" ).removeClass("display-modal-body");
  $('#DeleteButton').hide();
  $('#MoveButton').hide();
  $('#EditButton').hide();
  FinancialStatementLoad();
});
</script>

<!--- Asset --->
<cfif isDefined("data.DBASSET_ID")>
  <script type="text/javascript">
    $('#EditButton').click(function(e){
      e.preventDefault();
      $('.modal-backdrop').remove();
      $('.modal').modal('hide');
      $.post("financialStatement/balanceSheet/PL_Form_asset_edit.cfm", function(data){ 
          $( "#modalBox" ).html( data );
      });
    });
  </script>

<!--- Liability --->
<cfelseif isDefined("data.DBLIABILITY_ID")>

  <script type="text/javascript">
    $('#EditButton').click(function(e){
      e.preventDefault();
      $('.modal-backdrop').remove();
      $('.modal').modal('hide');
      $.post("financialStatement/balanceSheet/PL_Form_liability_edit.cfm", function(data){ 
          $( "#modalBox" ).html( data );
      });
    });

  </script>
</cfif>




<style type="text/css">
.display-modal-body {
padding: 0px;
}
.display-modal-footer {
margin-top: 0px;
}
.display-table{
  margin-bottom: 0px;
  border-color: #3f3f3f;
}
</style>