<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - This file is the mode bar, which acts as a filter for the financial statement data or the reports data.
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 06/09/2014
////////////////////////////////////////////////////// --->
<cfset session.user.userCurrentFSID = session.user.userRootFSID>
<div class="container">
  <p class="navbar-text" style="margin-left: 0px;"><span class="glyphicon glyphicon-filter"></span> Filter</a></p>
<!--- Mode buttons--->
  <form class="navbar-form navbar-left" role="form" method="post" action="" id="modeForm">
    <cfoutput>
      <input type="hidden" name="FSID" value='0' id="FSID">
      <input type="hidden" name="FSNavFunction" value='0' id="FSNavFunction">
    </cfoutput>
    <!--- Period radio buttons --->
    <div class="btn-group" data-toggle="buttons">
      <label for="PeriodUnitID" class="btn btn-default">
        <!--- Period Unit Selection--->
        <input type="radio" name="PeriodUnitID" class="radio" value="1" checked="false"/> 
        Monthly
      </label>
      <label for="PeriodUnitID" class="btn btn-default">
        <!--- Period Unit Selection--->
        <input type="radio" name="PeriodUnitID" class="radio" value="2" checked="false"/> 
        Yearly
      </label>
      <label for="PeriodUnitID" class="btn btn-default active">
        <!--- Period Unit Selection--->
        <input type="radio" name="PeriodUnitID" class="radio" value="3" checked="true"/> 
        All time
      </label>
    </div><!--- /.Period radio buttons --->

      <!--- Month select --->
      <select class="form-control" name="ModeMonth" id="monthSelect" disabled>
        <cfset months = ["January","February","March","April","May","June","July","August","September","Octover","November","December"]>
        <cfset k = 1>
        <cfloop index="names" array=#months#>
          <cfoutput><option value="#k#">#names#</option></cfoutput>
          <cfset k = k + 1>
        </cfloop>
      </select>
      <!--- Year Select --->
      <select class="form-control" name="ModeYear" id="yearSelect" disabled>
        <option value="2014">2014</option>
      </select>
  </form>
  <div id="navBar">
    <p class="navbar-text" >Navigation</a></p>
    <button type="button" class="btn btn-default navbar-btn" id="upButton"><span class="glyphicon glyphicon-chevron-up"></span> Up</button>
    <button type="button" class="btn btn-default navbar-btn" id="rootButton"><span class="glyphicon glyphicon-tree-deciduous"></span> Root</button>
  </div>
</div>

<cfinclude template="DL_Mtd_FS_EventHandler.cfm">