<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - This file has all the financial statement structure, it is only a tamplate that later fills itself with the users data.
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 31/05/2014
// -Hector Saul Ortigoza Mendez,   Mexico. 06/09/2014
////////////////////////////////////////////////////// --->


<!--- Financial Statement title--->
<div class="row" id="financialStatementTitle">
    <h1 id="financialStatementText"><span id="FSTitleSpan">Saul Ortigoza</span>'s Financial Statement</h1>
  Click on the financial statement title to display its data.
</div>

<div class="row">
  <!--- Income Statement --->
  <div class="col-md-6" id="incomeStatementContainer">
    <h2>Income Statement</h2>
    <!--- Incomes --->
    <div id="incomesContainer" class="financialStatementSubContainer">
      <div class="financialStatementHeadind">Incomes</div>
        <div id="incomesContent">
          <div class="loading" style="text-align: center;"> Loading.....</div>
        </div>
        <div id="incomesAdd" class="statementAdd" path="financialStatement\incomeStatement\PL_Form_income_add.cfm"><button type="button" class="btn btn-default btn-sm statementAddButton"><span class="glyphicon glyphicon-plus"></span> Add</button></div>
    </div><!--- /.Incomes --->

    <!--- Expenses --->
    <div id="expensesContainer" class="financialStatementSubContainer">
      <div class="financialStatementHeadind">Expenses</div>
        <div id="expensesContent">
          <div class="loading" style="text-align: center;"> Loading.....</div>
        </div>
        <div id="expensesAdd" class="statementAdd" path="financialStatement\incomeStatement\PL_Form_expense_add.cfm"><button type="button" class="btn btn-default btn-sm statementAddButton"><span class="glyphicon glyphicon-plus"></span> Add</button></div>
      </div>
    </div><!--- /.Expenses --->

  <!--- Summary --->
  <div class="col-md-6" id="summaryContainer">
    <h2>Summary</h2>
    <div class="financialStatementSubContainer" id="summaryContent">
      <div class="loading" style="text-align: center;"> Loading.....</div>
    </div>
  </div><!--- /.Summary --->
</div><!--- /.Row --->


<!--- Balance Sheet --->
<div id="balanceSheetContainer">
  <div class="row" style="text-align: center;"><h2>Balance Sheet</h2></div>
  <div class="row">
    <div class="col-md-6">
      <!--- Assets --->
      <div class="financialStatementSubContainer" id="assetsContainer">
        <div class="financialStatementHeadind">Assets</div>
        <div id="assetsContent">
          <div class="loading" style="text-align: center;"> Loading.....</div>
        </div>
        <div id="assetsAdd" class="statementAdd" path="financialStatement\balanceSheet\PL_Form_asset_add.cfm"><button type="button" class="btn btn-default btn-sm statementAddButton"><span class="glyphicon glyphicon-plus"></span> Add</button></div>
      </div><!--- /.Assets --->
    </div>

    <div class="col-md-6">
      <!--- Liabilities --->
      <div class="financialStatementSubContainer" id="liabilitiesContainer">
        <div class="financialStatementHeadind">Liabilities</div>
        <div id="liabilitiesContent">
          <div class="loading" style="text-align: center;"> Loading.....</div>
        </div>
        <div id="liabilitiesAdd" class="statementAdd" path="financialStatement\balanceSheet\PL_Form_liability_add.cfm"><button type="button" class="btn btn-default btn-sm statementAddButton"><span class="glyphicon glyphicon-plus"></span> Add</button></div>
      </div><!--- /.Liabilities --->
    </div>
  </div>
</div><!--- /.Balance Sheet --->

<!--- ///// Scripts ///// --->
<script type="text/javascript">
$( document ).ready(function() {
  $('#FSTitleSpan').html('<cfoutput>#session.user.userName#</cfoutput>');
  FinancialStatementLoad();
});

//Click an add button event
$('.statementAddButton').click(function(e){
  e.preventDefault();
  
  $('#EventHandlerDebug').html("Click an add button event");
  path = $(this).parent().attr('path');
  /*
  $('#blinde').show();
  $.post( path, function( data ) {
    $( "#blindeMainContent" ).html( data );
  });
  /*Temporal
  */
  $.post( path, function(data){ 
                $( "#modalBox" ).html( data ); 
            }
        );
});

//financial statement text click event
$('#financialStatementText').click(function(e){
  e.preventDefault();
  $('#EventHandlerDebug').html("financial statement text click event");

  $.post( "financialStatement/balanceSheet/balanceSheet_display.cfm", function( data ) {
    $( "#modalBox" ).html( data );
  });
});
</script>