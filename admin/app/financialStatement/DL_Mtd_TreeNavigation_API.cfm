<cfset finman = CreateObject("cfc\financialStManager") />
<cfdump var = #form#>
<cfif #form.FSNavFunction# EQ "Load_UP">
	<cfset getFS = finman.Retrieve_FS_data(#session.user.userCurrentFSID#)>
	<cfif #getFS.DBFINANCIALST_PARENTFS_ID# NEQ 0>
		<cfset session.user.userCurrentFSID = #getFS.DBFINANCIALST_PARENTFS_ID#>
		<cfset getFS = finman.Retrieve_FS_data(#session.user.userCurrentFSID#)>
		<script type="text/javascript">
			$('#ISIDinput').val(<cfoutput>#getFS.DBFINANCIALST_ID#</cfoutput>);
			$('#FSTitleSpan').html('<cfoutput>#getFS.DBFINANCIALST_NAME#</cfoutput>');
		  	window.scrollTo(0, 0);
		</script>
	</cfif>
</cfif><!---./Load_UP --->

<cfif #form.FSNavFunction# EQ "Load_Root">
	<cfset session.user.userCurrentFSID = #session.user.userRootFSID#>
	<cfset getFS = finman.Retrieve_FS_data(#session.user.userCurrentFSID#)>
	<script type="text/javascript">
		$('#ISIDinput').val(<cfoutput>#getFS.DBFINANCIALST_ID#</cfoutput>);
		$('#FSTitleSpan').html('<cfoutput>#getFS.DBFINANCIALST_NAME#</cfoutput>');
	  	window.scrollTo(0, 0);
	</script>
</cfif><!---./Load_Root --->

<cfif #form.FSNavFunction# EQ "Load_FS">
	<cfset session.user.userCurrentFSID = #form.FSID#>
	<cfset getFS = finman.Retrieve_FS_data(#session.user.userCurrentFSID#)>
	<script type="text/javascript">
  		$('#FSTitleSpan').html('<cfoutput>#getFS.DBFINANCIALST_NAME#</cfoutput>');
  		window.scrollTo(0, 0);
	</script>
</cfif><!---./Load_FS --->

<cfif #form.FSNavFunction# EQ "Refresh_Filter">
	<cfif NOT isDefined("form.MODEYEAR")>
		<cfset form.MODEYEAR = 0>
	</cfif>
	<cfif NOT isDefined("form.MODEMONTH")>
		<cfset form.MODEMONTH = 0>
	</cfif>
	<cfset session.user.userCurrentFilter = [#form.PERIODUNITID#, #form.MODEYEAR#,  #form.MODEMONTH#]>
	<cfdump var = #session.user.userCurrentFilter#>
</cfif><!---./Refresh_Filter --->