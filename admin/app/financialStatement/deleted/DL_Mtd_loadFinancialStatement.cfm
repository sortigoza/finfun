<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - <text>
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 31/05/2014
////////////////////////////////////////////////////// --->
<!---<cfdump var = #form#>--->
<cfset finman = CreateObject("cfc\financialStManager") />
<cfset session.user.userCurrentFSID = #form.FSID#>
<cfset getFS = finman.Retrieve_FS_data(#session.user.userCurrentFSID#)>
<!---
<cfdump var = #form#><br>
--->
<script type="text/javascript">
$( document ).ready(function() {
  $('#FSTitleSpan').html('<cfoutput>#getFS.DBFINANCIALST_NAME#</cfoutput>');
  window.scrollTo(0, 0);
  FinancialStatementLoad();
});
</script>
