<script type="text/javascript">
// financialStatementLoad(Period, From, To)
function FinancialStatementLoad(){
  $.post( "financialStatement/incomeStatement/incomeStatement_incomes.cfm", function( data ) {
    $( "#incomesContent" ).html( data );
  });
  $.post( "financialStatement/incomeStatement/incomeStatement_expenses.cfm", function( data ) {
    $( "#expensesContent" ).html( data );
  });
  $.post( "financialStatement/balanceSheet/balanceSheet_assets.cfm", function( data ) {
    $( "#assetsContent" ).html( data );
  });
  $.post( "financialStatement/balanceSheet/balanceSheet_Liabilities.cfm", function( data ) {
    $( "#liabilitiesContent" ).html( data );
  });
  $.post( "financialStatement/Summary/Summary.cfm", function( data ) {
    $( "#summaryContent" ).html( data );
  });
}

//Click root button event
$('#rootButton').click(function(e){
  e.preventDefault();
  $.msgbox("Click root button event");
  location.reload();
  $.post( "financialStatement/DL_Mtd_TreeNavigation_API.cfm", modeFormData, function( data ) {
    $( "#modalBox" ).html( data );
  });
  FinancialStatementLoad();
});

//Click up button event
$('#upButton').click(function(e){
  e.preventDefault();
  $.msgbox("Click up button event");
  var modeFormData = $('#modeForm').serialize();

  $.post( "financialStatement/DL_Mtd_TreeNavigation_API.cfm", modeFormData, function( data ) {
    $( "#modalBox" ).html( data );
  });
  FinancialStatementLoad();
});

//Click an add button event
$('.statementAddButton').click(function(e){
  e.preventDefault();
  $.msgbox("Click an add button event");
  $('#blinde').show();
  path = $(this).parent().attr('path');
  $.post( path, function( data ) {
    $( "#blindeMainContent" ).html( data );
  });
});

//financial statement text click event
$('#financialStatementText').click(function(e){
  e.preventDefault();
  $.msgbox("financial statement text click event");
  var modeFormData = $('#modeForm').serialize();

  $.post( "financialStatement/balanceSheet/balanceSheet_display.cfm", modeFormData, function( data ) {
    $( "#modalBox" ).html( data );
  });
});

//Modal form change event 
$('#modeForm').change(function() {
  $.msgbox("Modal form change event ");
  var modeFormData = $(this).serialize();
  var modeval = this.PeriodUnitID.value;
  //console.log( modeFormData );

  switch(modeval) {
    case '3':
        $('#monthSelect').attr('disabled',true);
        $('#yearSelect').attr('disabled',true);
        break;
    case '2':
        $('#monthSelect').attr('disabled',true);
        $('#yearSelect').attr('disabled',false);
        break;
    case '1':
        $('#monthSelect').attr('disabled',false);
        $('#yearSelect').attr('disabled',false);
        break;
  }

  $.post( "financialStatement/DL_Mtd_loadFinancialStatement.cfm", modeFormData, function( data ) {
    $( "#modalBox" ).html( data );
  });
});
</script>

Tree Event Handler Loaded!