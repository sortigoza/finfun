// financialStatementLoad(Period, From, To)
function FinancialStatementLoad(){
  $.post( "financialStatement/incomeStatement/incomeStatement_incomes.cfm", function( data ) {
    $( "#incomesContent" ).html( data );
  });
  $.post( "financialStatement/incomeStatement/incomeStatement_expenses.cfm", function( data ) {
    $( "#expensesContent" ).html( data );
  });
  $.post( "financialStatement/balanceSheet/balanceSheet_assets.cfm", function( data ) {
    $( "#assetsContent" ).html( data );
  });
  $.post( "financialStatement/balanceSheet/balanceSheet_Liabilities.cfm", function( data ) {
    $( "#liabilitiesContent" ).html( data );
  });
  $.post( "financialStatement/Summary/Summary.cfm", function( data ) {
    $( "#summaryContent" ).html( data );
  });
}