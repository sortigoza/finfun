<cfif #session.user.userTempVar1# EQ "income">
<cfset finman = CreateObject("cfc\financialStManager") />
<cfset data = finman.Retrieve_income_data(#session.user.userTempVar2#)>

<div class="modal fade" id="IncomeEdit_Modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <cfoutput query="data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <div class="modal-title">Income Edit: #DBINCOME_NAME#</div>
        </div>
        <div class="modal-body">     

          <form class="form-horizontal incomeForm" role="form" method="post" action="" id="incomeForm">
            <input type="hidden" name="FSMFunction" value="IncomeStatement_Update_Income"/>
            <!--- Income ID --->
            <input type="hidden" id="Income_ID" name="Income_ID" 
              value="#DBINCOME_ID#"
            />
            <!--- FS ID --->
            <input type="hidden" name="FinancialSt_ID" 
              value="#DBINCOME_ID#"
            />
            <!--- Name --->
            <div class="form-group">
              <label for="incomeName" class="col-sm-2 control-label">Income Name: </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="incomeName" placeholder="Income Name" required="true" name="incomeName" maxlength="60"
                  value="#DBINCOME_NAME#"
                />
              </div>
            </div>
            <!--- Description --->
            <div class="form-group">
              <label for="Description" class="col-sm-2 control-label">Description: </label>
              <div class="col-sm-10">
                <textarea type="text" class="form-control" id="Description" placeholder="Description" rows="3" required="true" name="incomeDescription">#DBINCOME_DESCRIPTION#</textarea>
              </div>
            </div>
            <!--- Amount --->
            <div class="form-group">
              <label for="Amount" class="col-sm-2 control-label">Amount: </label>
              <div class="col-sm-10">
                <input type="number" step="any" class="form-control" id="Amount" placeholder="Amount" required="true" name="incomeAmount"
                  value="#DBINCOME_AMOUNT#"
                />
              </div>
            </div>
            <!--- Date --->
            <div class="form-group">
              <label for="Date" class="col-sm-2 control-label">Date: </label>
              <div class="col-sm-10">
                <input type="date" class="form-control" id="Date" placeholder="Date" required="true" name="incomeDate"
                  value="#DateFormat(DBINCOME_CREATIONDATE, "YYYY-MM-DD")#"
                />
              </div>
            </div>
            <!--- Category --->
            <!--- Submit button 
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default" id="saveincome">Submit </button>
              </div>
            </div> --->
          </form>
        </cfoutput>

        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default" id="saveChangesButton"><span class="glyphicon glyphicon-pencil"></span> Save Changes</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="cancelButton"><span class="glyphicon glyphicon-minus"></span> Cancel</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$( document ).ready(function() {
  $('.modal').modal('show');
});
</script>

<script type="text/javascript">

$( '#saveChangesButton' ).click(function(e) {
  e.preventDefault();
  var incomeFormData = $('#incomeForm').serialize();
  //console.log( 'form data: ' + $( "#incomeForm" ).serialize() );

  $.post("/cfc/financialStManager_API.cfm", incomeFormData , function( data ) {
    $('.modal-body').empty();
    $('.modal-body').append(data);
  })
    .fail(function() {
    $('.modal-body').empty();
    $('.modal-body').append('<span class="glyphicon glyphicon-remove-sign"></span> Error! Try again later...!<br/>');
  });

  $('#saveChangesButton').hide();
  $('#cancelButton').html('<span class="glyphicon glyphicon-minus"></span> Close');
  
   FinancialStatementLoad();
});
</script>

</cfif>