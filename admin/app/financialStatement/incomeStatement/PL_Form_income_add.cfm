<div class="modal fade" id="IncomeAdd_Modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div class="modal-title">Income Add</div>
      </div><!-- /.modal-header -->
      <div class="modal-body">    
        
        <form class="form-horizontal incomeForm" role="form" method="post" action="" id="incomeForm">
          <input type="hidden" name="FSMFunction" value="IncomeStatement_Add_Income"/>
          <!--- Current FS ID --->
          <input type="hidden" name="FinancialSt_ID" value="<cfoutput>#session.user.userCurrentFSID#</cfoutput>"/>
          <!--- Name --->
          <div class="form-group">
            <label for="incomeName" class="col-sm-2 control-label">Income Name: </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="incomeName" placeholder="Income Name" required="true" name="incomeName" maxlength="60">
            </div>
          </div>
          <!--- Description --->
          <div class="form-group">
            <label for="Description" class="col-sm-2 control-label">Description: </label>
            <div class="col-sm-10">
              <textarea type="text" class="form-control" id="Description" placeholder="Description" rows="3" required="false" name="incomeDescription"></textarea>
            </div>
          </div>
          <!--- Amount --->
          <div class="form-group">
            <label for="Amount" class="col-sm-2 control-label">Amount: </label>
            <div class="col-sm-5">
              <input type="number" step="any" class="form-control" id="Amount" placeholder="Amount" required="true" name="incomeAmount">
            </div>
          </div>
          <!--- Date --->
          <div class="form-group">
            <label for="Date" class="col-sm-2 control-label">Date: </label>
            <div class="col-sm-5">
              <input type="date" class="form-control" id="Date" placeholder="Date" required="true" name="incomeDate" value="<cfoutput>#DateFormat(Now(), "YYYY-MM-DD")#</cfoutput>">
            </div>
          </div>
          
          <!--- Tags --->
          <div class="form-group">
            <label for="Tags" class="col-sm-2 control-label">Tags: </label>
            <div class="col-sm-5">
              <input class="form-control" name="incomeTags"  placeholder="Tags">
            </div>
          </div>
          <!--- Category --->
          <div class="form-group">
          <label for="Category" class="col-sm-2 control-label">Category: </label>
            <div class="col-sm-5">
              <input class="form-control" name="incomeCategory" placeholder="Category">
            </div>
          </div>
        </form><!-- /.form -->
        </div><!-- /.modal-body -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-default" id="addButton"><span class="glyphicon glyphicon-plus"></span> Add Income</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="cancelButton"><span class="glyphicon glyphicon-minus"></span> Cancel</button>
      </div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$( document ).ready(function() {
  $('.modal').modal('show');
});

$( '#addButton' ).click(function(e) {
  e.preventDefault();
  var incomeFormData = $('#incomeForm').serialize();
  //console.log(incomeFormData );
  $.post("/cfc/financialStManager_API.cfm", incomeFormData , function( data ) {
    $('.modal-body').empty();
    $('.modal-body').append(data);
  })
    .fail(function() {
    $('.modal-body').empty();
    $('.modal-body').append('<span class="glyphicon glyphicon-remove-sign"></span> Error! Try again later...!<br/>');
  });

  $('#addButton').hide();
  $('#cancelButton').html('<span class="glyphicon glyphicon-minus"></span> Close');
  FinancialStatementLoad();
});
</script>

