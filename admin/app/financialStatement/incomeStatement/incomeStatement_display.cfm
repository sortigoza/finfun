<cfset finman = CreateObject("cfc\financialStManager") />
<!--- Type: 1 = income, 2 = expense --->

<div class="modal fade" id="incomeStDisplay_Modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <cfif #form.type# EQ 1>
        <!--- Income --->
        <cfset data = finman.Retrieve_income_data(#form.INCOMEID#)>
        <cfset session.user.userTempVar1 = "income"><!--- Internal interchange of data --->
        <cfset session.user.userTempVar2 = #data.DBINCOME_ID#><!--- Internal interchange of data --->
        <cfoutput query="data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <div class="modal-title">Income Display: #DBINCOME_NAME#</div>
        </div>
        <div class="modal-body display-modal-body">    
          <table class="table table-bordered display-table">
            <tr bgcolor="##e0e0e0">
              <td>Name</td><td>Income ID</td><td>Amount</td><td>Date</td>
            </tr>
            <tr>
              <td>#DBINCOME_NAME#</td><td>#DBINCOME_ID#</td><td>#DBINCOME_AMOUNT#</td><td>#DateFormat(DBINCOME_CREATIONDATE, "ddd, mmmm dd, yyyy")#</td>
            </tr>
            <tr>
              <td colspan="4" bgcolor="##e0e0e0">Description</td>
            </tr>
            <tr>
              <td colspan="4">#DBINCOME_DESCRIPTION#</td>
            </tr>
            <tr>
              <td bgcolor="##e0e0e0">Category</td><td colspan="3">#DBINCOME_CATEGORY_ID#</td>
            </tr>
          </table>  
        </div>
      </cfoutput>

      <cfelseif #form.type# EQ 2>
        <!--- Expense --->
        <cfset data = finman.Retrieve_expense_data(#form.EXPENSEID#)>
        <cfset session.user.userTempVar1 = "expense"><!--- Internal interchange of data --->
        <cfset session.user.userTempVar2 = #data.DBEXPENSE_ID#><!--- Internal interchange of data --->
        <cfoutput query="data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <div class="modal-title">Expense Display: #DBEXPENSE_NAME#</div>
        </div>
        <div class="modal-body display-modal-body">
          <table class="table table-bordered display-table">
            <tr bgcolor="##e0e0e0">
              <td>Name</td><td>Expense ID</td><td>Amount</td><td>Date</td>
            </tr>
            <tr>
              <td>#DBEXPENSE_NAME#</td><td>#DBEXPENSE_ID#</td><td>#DBEXPENSE_AMOUNT#</td><td>#DateFormat(DBEXPENSE_CREATIONDATE, "ddd, mmmm dd, yyyy")#</td>
            </tr>
            <tr>
              <td colspan="4" bgcolor="##e0e0e0">Description</td>
            </tr>
            <tr>
              <td colspan="4">#DBEXPENSE_DESCRIPTION#</td>
            </tr>
            <tr>
              <td bgcolor="##e0e0e0">Category</td><td colspan="3">#DBEXPENSE_CATEGORY_ID#</td>
            </tr>
          </table>   
        </div>
        </cfoutput>

      </cfif><!-- /.cfif -->

      <div class="modal-footer display-modal-footer">
        <button type="button" class="btn btn-default" id="DeleteButton"><span class="glyphicon glyphicon-trash"></span> Delete</button>
        <button type="button" class="btn btn-default" id="MoveButton" disabled="disabled"><span class="glyphicon glyphicon-share-alt"></span> Move</button>
        <button type="button" class="btn btn-default" id="EditButton"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-minus"></span> Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
$( document ).ready(function() {
  $('.modal').modal('show');
});

$('#DeleteButton').click(function(e){
  e.preventDefault();
  $.post("/cfc/financialStManager_API.cfm", { FSMFunction: "IncomeStatement_Delete_Income_Expense"}, function( data ) {
    $( ".modal-body" ).html( data );
  });
  $( ".modal-body" ).removeClass("display-modal-body");
  $('#DeleteButton').hide();
  $('#MoveButton').hide();
  $('#EditButton').hide();
  FinancialStatementLoad();
});
</script>

<cfif #form.type# EQ 1>
  <script type="text/javascript">
    $('#EditButton').click(function(e){
      e.preventDefault();
      $('.modal-backdrop').remove();
      $('.modal').modal('hide');
      $.post("financialStatement/incomeStatement/PL_Form_income_edit.cfm", function(data){ 
          $( "#modalBox" ).html( data );
      });
    });
  </script>

<cfelseif #form.type# EQ 2>
  <script type="text/javascript">
    $('#EditButton').click(function(e){
      e.preventDefault();
      $('.modal-backdrop').remove();
      $('.modal').modal('hide');
      $.post("financialStatement/incomeStatement/PL_Form_expense_edit.cfm", function(data){ 
          $( "#modalBox" ).html( data );
      });
    });
  </script>
  
</cfif>

<style type="text/css">
.display-modal-body {
padding: 0px;
}
.display-modal-footer {
margin-top: 0px;
}
.display-table{
  margin-bottom: 0px;
  border-color: #3f3f3f;
}
</style>