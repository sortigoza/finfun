<!--- IncomeStatent_Income: this module shows the expenses of a time perios given the parent financial statement and dates from-to.--->
<cfset finman = CreateObject("cfc\financialStManager") />
<cfset finfunc = CreateObject("cfc\financialStFunctions") />
<cfset getexpenses = finman.Get_expenses(#session.user.userCurrentFSID#)>
<cfset getliabilities = finman.Get_liabilities(#session.user.userCurrentFSID#)>
<cfset lastCurrentRow = 0>

<table class="table table-hover" id="expenses_table">
    <thead>
        <tr>
            <th width="5%"><cfoutput>##</cfoutput></th>
            <th width="70%">Name</th>
            <th width="25%">Amount</th>
        </tr>
    </thead>
    <tbody>
        <cfoutput query="getexpenses">
            <tr expenseID="#DBEXPENSE_ID#">
                <td>#CurrentRow#</td>
                <td>#DBEXPENSE_NAME#</td>
                <td class="expenseAmount">#DBEXPENSE_AMOUNT#</td>
            </tr>
            <cfset lastCurrentRow = #CurrentRow#>
        </cfoutput>
        <cfoutput query="getliabilities">
            <tr incomeID="liability#CurrentRow#">
                <td>#CurrentRow + lastCurrentRow#</td>
                <td>#DBLIABILITY_NAME#</td>
                <cfset incomeAlldepth = finfunc.Get_NetIncome_All_Depth(#DBLIABILITY_CHILDFS_ID#)>
                <td class="expenseAmount">#NumberFormat(-1 *incomeAlldepth,"_-___.__")#</td>
            </tr>
        </cfoutput>
    </tbody>
</table>

<script type="text/javascript">
    $('#expenses_table tr').click(function(e){
        e.preventDefault();
        var expenseID = $(this).attr('expenseID');  
        //alert(expenseID);
        $.post( 'financialStatement/incomeStatement/incomeStatement_display.cfm', {type: 2, expenseID: expenseID}, function(data){ $( "#modalBox" ).html( data ); });
    });
</script>