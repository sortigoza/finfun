<!--- IncomeStatent_Income: this module shows the incomes of a time perios given the parent financial statement and dates from-to.--->
<cfset finman = CreateObject("cfc\financialStManager") />
<cfset finfunc = CreateObject("cfc\financialStFunctions") />
<cfset getincomes = finman.Get_incomes(#session.user.userCurrentFSID#)>
<cfset getassets = finman.Get_assets(#session.user.userCurrentFSID#)>
<cfset lastCurrentRow = 0>

<table class="table table-hover" id="incomes_table">
    <thead>
        <tr>
            <th width="5%"><cfoutput>##</cfoutput></th>
            <th width="70%">Name</th>
            <th width="25%">Amount</th>
        </tr>
    </thead>
    <tbody>
        <cfoutput query="getincomes">
            <tr incomeID="#DBINCOME_ID#">
                <td>#CurrentRow#</td>
                <td>#DBINCOME_NAME#</td>
                <td class="incomeAmount">#NumberFormat(DBINCOME_AMOUNT,"-_,___.__")#</td>
            </tr>
            <cfset lastCurrentRow = #CurrentRow#>
        </cfoutput>
        <cfoutput query="getassets">
            <tr incomeID="asset#CurrentRow#">
                <td>#CurrentRow + lastCurrentRow#</td>
                <td>#DBASSET_NAME#</td>
                <cfset incomeAlldepth = finfunc.Get_NetIncome_All_Depth(#DBASSET_CHILDFS_ID#)>
                <td class="incomeAmount">#NumberFormat(incomeAlldepth,"-_,___.__")#</td>
            </tr>
        </cfoutput>
    </tbody>
</table>

<script type="text/javascript">
    $('#incomes_table tr').click(function(e){
        e.preventDefault();

        var incomeID = $(this).attr('incomeID');  

        $.post( 'financialStatement/incomeStatement/incomeStatement_display.cfm', {type: 1, incomeID: incomeID}, function(data){ 
                $( "#modalBox" ).html( data ); 
            }
        );
    });
</script>