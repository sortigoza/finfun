<cfset finman = CreateObject("cfc\financialStManager") />
<cfset data = finman.Retrieve_expense_data(#form.EXPENSEID#)>

<cfoutput query="data">

<div class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Expense Display: #DBEXPENSE_NAME#</h4>
      </div>
      <div class="modal-body">
        
			
				Name: #DBEXPENSE_NAME# <br>
				Expense ID: #DBEXPENSE_ID# <br> 
				Amount: #DBEXPENSE_AMOUNT# <br>
				Date: #DateFormat(DBEXPENSE_CREATIONDATE, "ddd, mmmm dd, yyyy")# <br>
				Category: #DBEXPENSE_CATEGORY_ID# <br><br>
				Description: <br>
				#DBEXPENSE_DESCRIPTION# <br><br>		
			

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</cfoutput>

<script type="text/javascript">
$( document ).ready(function() {
  $('.modal').modal('toggle');
});
	
</script>