<cfset finman = CreateObject("cfc\financialStManager") />
<!--- Add funcionality "Q_User_Allowed() --->

<!---  in case of call of posting an income, create income. --->
<cfif IsDefined("form.INCOMENAME")>
	<cfif IsDefined("form.INCOME_ID")>
		<cfdump var=#form#>
		<span class="glyphicon glyphicon-ok"></span> Your Income, with ID: <cfoutput>incomeID</cfoutput>, was successfuly updated!
	<cfelse>
		<cfset incomeID = finman.Create_income(#form.INCOMENAME#, #form.FINANCIALST_ID#, #form.INCOMEDESCRIPTION#, #form.INCOMEAMOUNT#, #form.INCOMEDATE#)>
		<span class="glyphicon glyphicon-ok"></span> Your Income, with ID: <cfoutput>#incomeID#</cfoutput>, was successfuly added to te Database!
	</cfif>
</cfif>

<!---  in case of call of posting an expense, create expense. --->
<cfif IsDefined("form.EXPENSENAME")>
	<cfif IsDefined("form.EXPENSE_ID")>
		<cfdump var=#form#>
		<span class="glyphicon glyphicon-ok"></span> Your Expense, with ID: <cfoutput>expenseID</cfoutput>, was successfuly updated!
	<cfelse>
		<cfset expenseID = finman.Create_expense(#form.EXPENSENAME#, #form.FINANCIALST_ID#, #form.EXPENSEDESCRIPTION#, #form.EXPENSEAMOUNT#, #form.EXPENSEDATE#)>
		<span class="glyphicon glyphicon-ok"></span> Your Expense, with ID: <cfoutput>#expenseID#</cfoutput>, was successfuly added to te Database!
	</cfif>
</cfif>

