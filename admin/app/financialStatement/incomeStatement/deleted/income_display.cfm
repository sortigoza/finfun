<cfset finman = CreateObject("cfc\financialStManager") />
<cfset data = finman.Retrieve_income_data(#form.INCOMEID#)>


<cfoutput query="data">

<div class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Income Display: #DBINCOME_NAME#</h4>
      </div>
      <div class="modal-body">
        dump
			   <cfdump var = #form#>
				Name: #DBINCOME_NAME# <br>
				Income ID: #DBINCOME_ID# <br> 
				Amount: #DBINCOME_AMOUNT# <br>
				Date: #DateFormat(DBINCOME_CREATIONDATE, "ddd, mmmm dd, yyyy")# <br>
				Category: #DBINCOME_CATEGORY_ID# <br><br>
				Description: <br>
				#DBINCOME_DESCRIPTION# <br><br>		
			

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</cfoutput>

<script type="text/javascript">
$( document ).ready(function() {
  $('.modal').modal('toggle');
});
	
</script>