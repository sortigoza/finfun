<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - <text>
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 31/05/2014
////////////////////////////////////////////////////// --->
<cfset finman = CreateObject("cfc\financialStManager") />
<!--- Type: 1 = income, 2 = expense --->
<cfif #session.user.userTempVar1# EQ "income">
	<cfset return = finman.Delete_income(#session.user.userTempVar2#)>
<cfelseif #session.user.userTempVar1# EQ "expense">
	<cfset return = finman.Delete_expense(#session.user.userTempVar2#)>
</cfif>
<cfset session.user.userTempVar1 = 0>
<cfset session.user.userTempVar2 = 0>
<cfif return EQ 1>
	<span class="glyphicon glyphicon-ok"></span> Deleted Successfully!
</cfif>
