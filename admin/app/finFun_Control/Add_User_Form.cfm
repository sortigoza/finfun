<form class="form" role="form" method="post" action="" id="addUserForm">

    <div class="form-group">
      <label >Add user... </label>
    </div>
    <!--- userName --->
    <div class="form-group">
      <label for="userName" class="col-sm-2 control-label">userName: </label>
      <input type="text" class="form-control" id="userName" placeholder="userName" required="true" name="userName">
    </div>
    <!--- userEmail --->
    <div class="form-group">
      <label for="userEmail" class="col-sm-2 control-label">userEmail: </label>
      <input type="text" class="form-control" id="userEmail" placeholder="userEmail" required="true" name="userEmail">
    </div>
    <!--- userUserName --->
    <div class="form-group">
      <label for="userUserName" class="col-sm-2 control-label">userUserName: </label>
      <input type="text" class="form-control" id="userUserName" placeholder="userUserName" required="true" name="userUserName">
    </div>
    <!--- userPassword --->
    <div class="form-group">
      <label for="userPassword" class="col-sm-2 control-label">userPassword: </label>
      <input type="text" class="form-control" id="userPassword" placeholder="userPassword" required="true" name="userPassword">
    </div>
    <!--- Submit button --->
    <div class="form-group">
     <button type="submit" class="btn btn-default" id="saveIncome">Submit </button>
    </div>
</form>



<script type="text/javascript">

$('#addUserForm' ).submit(function(e) {
  e.preventDefault();
  var addUserFormData = $(this).serialize();
  $.post("finFun_Control/Add_User_Method.cfm", addUserFormData, function(data){ 
    $( "#main_content" ).html( data ); 
  });
});

</script>