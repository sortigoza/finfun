<style type="text/css">

#mainContainer{
	margin-top: 53px;
}

#navigation {
width: 200px;
height: 100%;
margin: 0;
background-color: #131410;

position: fixed;
padding-right: 1px;
padding-left: 3px;
color: #E5E5E5;
}

#rigth_wraper{
margin-left:200px;
}

#main_content{
padding: 40px;
padding-top: 60px;
min-height: 500px;
background-image:url('/assets/textures/cream_dust.png');
background-repeat: repeat;
}

</style>

<div id="mainContainer">
	<div id="navigation"> 
		<table class="table table-hover" id="navigation_table">
  			<tr functionID='1'><td>Create User</td></tr>
  			<tr functionID='2'><td>Delete User</td></tr>
  			<tr functionID='3'><td>Edit User</td></tr>
  			<tr functionID='4'><td>Users List</td></tr>
		</table>		
	</div><!-- fin navigation -->

	<div id="rigth_wraper">
		<div id="main_content" class="container-fluid">
			Main container
		</div>
		<footer class="container-fluid">
      		<p>&copy; Fin-Fun Company 2014</p>
    	</footer>
	</div><!-- fin rigth wraper-->
</div><!-- fin container-->

<script type="text/javascript">
    $('#navigation_table tr').click(function(e){
        e.preventDefault();

        switch($(this).attr('functionID')){
        	case '1':
	           	$.post('finFun_Control/Add_User_Form.cfm', function(data){ 
	           		$( "#main_content" ).html( data ); 
	           	});
	           	break;
	        case '4':
	           	$.post('finFun_Control/User_List.cfm', function(data){ 
	           		$( "#main_content" ).html( data ); 
	           	});
	           	break;
        }
    });

</script>