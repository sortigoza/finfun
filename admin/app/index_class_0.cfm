<!--- ////////////////////////////////////////////////////
// Copyright Fin-Fun Company 2014. All Rights Reserved. 

// <company address> 

//  The copyright to the computer program(s) herein
//  is the property of Fin-Fun Company, Mexico. The
//  program(s) may be used and/or copied only with the
//  written permission of Fin-Fun Company or in accordance
//  with the terms and conditions stipulated in the
//  agreement/contract under which the program(s) have
//  been supplied. This copyright notice must not be 
//  removed.


// File Description:
// - <text>
// 
// Authors: 
// -Hector Saul Ortigoza Mendez,   Mexico. 31/05/2014
////////////////////////////////////////////////////// --->

<!--- Barra de Navegacion --->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Fin-Fun</a>
        </div>
        <div class="navbar-collapse collapse">
          <!--- Nav Buttons --->
          <ul class="nav navbar-nav" id="navButtons">
            <li class="active"><a href="#finfunControl" id="finFunControlLink">FinFun Control</a></li>
            <li><a href="#finfunAnalyser" id="finFunAnalyserLink">FinFun Analyser</a></li>
          </ul>
          <!--- User log-in Form --->
          <button type="button" class="btn btn-default navbar-btn navbar-right" id="logoutButton"><span class="glyphicon glyphicon-off"></span> Logout</button>
          <cfoutput>
          <p class="navbar-text navbar-right">Signed in as administrator #session.user.userName# &nbsp;&nbsp;</p>
          </cfoutput>
          
        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <!-- Main Container -->
    <div id="mainContainer">
      <cfinclude template="finFun_Control/finfunControl_Index.cfm">
    </div>

<script type="text/javascript">
    $('#finFunControlLink').click(function(e){
        e.preventDefault();
        $(this).parent().addClass('active');
        $('#finFunAnalyserLink').parent().removeClass('active');

        $.post( "finFun_Control/finfunControl_Index.cfm", function( data ) { $( "#mainContainer" ).html( data ); });
    });
    $('#finFunAnalyserLink').click(function(e){
        e.preventDefault();
        $(this).parent().addClass('active');
        $('#finFunControlLink').parent().removeClass('active');

        $.post( "finFun_Analyser/finfunAnalyser_Index.cfm", function( data ) { $( "#mainContainer" ).html( data ); });
    });
</script>