<!--- Mode buttons--->
        <div class="row" id="modeButtons">
          <form class="form-inline" role="form" method="post" action="" id="modeForm">
            <div class="form-group col-md-6">
              <div class="btn-group" data-toggle="buttons">
                <label for="PeriodUnitID" class="btn btn-default active">
                  <!--- Period Unit Selection--->
                  <input type="radio" name="PeriodUnitID" class="radio" value="1" checked="true"/> 
                  Monthly
                </label>
                <label for="PeriodUnitID" class="btn btn-default">
                  <!--- Period Unit Selection--->
                  <input type="radio" name="PeriodUnitID" class="radio" value="2" checked="false"/> 
                  Yearly
                </label>
              </div>
            </div>
            <div class="form-group col-md-6" style="text-align: right;">
              <select class="form-control">
                <cfset months = ["January","February","March","April","May","June","July","August","September","Octover","November","December"]>
                <cfloop index="names" array=#months#>
                  <cfoutput><option>#names#</option></cfoutput>
                </cfloop>
              </select>
              <select class="form-control">
                <option>2014</option>
              </select>
            </div>
          </form>
        </div>